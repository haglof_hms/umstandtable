// SelectSpeciesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectSpeciesDlg.h"


// CSelectSpeciesDlg dialog

IMPLEMENT_DYNAMIC(CSelectSpeciesDlg, CDialog)

CSelectSpeciesDlg::CSelectSpeciesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSpeciesDlg::IDD, pParent)
{

}

CSelectSpeciesDlg::~CSelectSpeciesDlg()
{
}

void CSelectSpeciesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_LIST2, m_wndListCtrl);
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
}


BEGIN_MESSAGE_MAP(CSelectSpeciesDlg, CDialog)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDOK, &CSelectSpeciesDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CSelectSpeciesDlg message handlers

BOOL CSelectSpeciesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	//set db connection
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

	setLanguage();
	setListCtrl();
	

	return TRUE;
}

BOOL CSelectSpeciesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CSelectSpeciesDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}

//what to do if not connected?

	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}

void CSelectSpeciesDlg::OnBnClickedOk()
{
	int nID;
	CTransaction_species *pSpc;
	m_vecSpeciesSelected.clear();

	if(m_wndListCtrl.GetItemCount() > 0)
	{
		for(int i=0; i<m_wndListCtrl.GetItemCount(); i++)
		{
			nID = (int)m_wndListCtrl.GetItemData(i);
			if(m_wndListCtrl.GetCheck(i))
			{
				pSpc = getSpecie(nID);
				if(pSpc != NULL)
				{
					m_vecSpeciesSelected.push_back(*pSpc);
				}
			}
		}
	}

	OnOK();
}


void CSelectSpeciesDlg::setLanguage(void)
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING130));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING131));
			m_wndBtnOK.SetWindowText(xml->str(IDS_STRING135));
			m_wndBtnCancel.SetWindowText(xml->str(IDS_STRING136));
			m_csDBErrorMsg = xml->str(IDS_STRING167);
		}
		delete xml;
	}
}

void CSelectSpeciesDlg::setListCtrl(void)
{
	int nCnt = 0;
	CString csSpcId;

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_wndListCtrl.InsertColumn(0, xml->str(IDS_STRING132), LVCFMT_CENTER, 40);
			m_wndListCtrl.InsertColumn(1, xml->str(IDS_STRING133), LVCFMT_LEFT, 30);
			m_wndListCtrl.InsertColumn(2, xml->str(IDS_STRING134), LVCFMT_LEFT, 130);
		}
		delete xml;
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_wndHeaderCtrl.SubclassWindow(hWndHeader);
	//theme xp eller 2003?
	m_wndHeaderCtrl.SetTheme(new CXTHeaderCtrlThemeOffice2003());

	m_wndListCtrl.ModifyExtendedStyle(0, LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	//add species to listctrl
	if(m_bConnected)
	{
		getSpeciesFromDB(m_dbConnectionData, m_vecSpecies);
		
		if(m_vecSpecies.size() > 0)
		{
			for(UINT i=0; i<m_vecSpecies.size(); i++)
			{
				if(!isSpecieUsed(m_vecSpecies[i].getSpcID()))
				{
					csSpcId.Format(_T("%d"),m_vecSpecies[i].getSpcID());
					m_wndListCtrl.InsertItem(nCnt, _T(""),0);
					m_wndListCtrl.SetItem(nCnt, 1, LVIF_TEXT, csSpcId, 0, NULL, NULL, NULL);
					m_wndListCtrl.SetItem(nCnt, 2, LVIF_TEXT, m_vecSpecies[i].getSpcName(), 0, NULL, NULL, NULL);
					m_wndListCtrl.SetItemData(nCnt, m_vecSpecies[i].getSpcID());
					nCnt++;
				}//if(!isSpecieUsed(m_vecSpecies[i].getSpcID()))
			}//for(UINT i=0; i<m_vecSpecies.size(); i++)
		}//if(m_vecSpecies.size() > 0)
	}//if(m_bConnected)
	else
	{
		AfxMessageBox(m_csDBErrorMsg, MB_ICONEXCLAMATION);
		OnCancel();
	}
		
}

BOOL CSelectSpeciesDlg::isSpecieUsed(int spc_id)
{
	if(m_vecSpeciesUsed.size() == 0)
		return FALSE;

	for(UINT i=0; i<m_vecSpeciesUsed.size(); i++)
		if(m_vecSpeciesUsed[i] == spc_id)
			return TRUE;

	return FALSE;
}

CTransaction_species* CSelectSpeciesDlg::getSpecie(int spc_id)
{
	if(m_vecSpecies.size() > 0)
	{
		for(UINT i=0; i<m_vecSpecies.size(); i++)
		{
			if(m_vecSpecies[i].getSpcID() == spc_id)
				return &m_vecSpecies[i];
		}
	}

	return NULL;
}


