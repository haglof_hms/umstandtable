// StandTableView.cpp : implementation file
//

#include "stdafx.h"
#include "StandTableView.h"
#include "SelectSpeciesDlg.h"
#include "MDIStandTableFrame.h"
#include "SpeciesView.h"
#include "SampleTreeView.h"
#include "SpeciesViewReportRec.h"
#include "OpenFileFunctions.h"

// CStandTableView

IMPLEMENT_DYNCREATE(CStandTableView, CView)

CStandTableView::CStandTableView()
{
	
}

CStandTableView::~CStandTableView()
{
	//AfxMessageBox(_T("~CStandTableView()"));
}

BEGIN_MESSAGE_MAP(CStandTableView, CView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_MESSAGE(WM_USER_MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()


// CStandTableView drawing

void CStandTableView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}


// CStandTableView diagnostics

#ifdef _DEBUG
void CStandTableView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CStandTableView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CStandTableView message handlers


int CStandTableView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	
	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,m_sAbrevLangSet,LANGUAGE_FN_EXT);

	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//tab control
	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,CRect(0,0,0,0),this, IDC_TABCONTROL_1);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = FALSE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetImageManager()->SetIcons(IDB_TAB_ICONS,NULL,0,CSize(16, 16),xtpImageNormal);

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{	
			//create tabpages
			AddView(RUNTIME_CLASS(CTabPageManager),xml->str(IDS_STRING126),3);	//cut
			AddView(RUNTIME_CLASS(CTabPageManager),xml->str(IDS_STRING127),3);	//standing
			AddView(RUNTIME_CLASS(CTabPageManager),xml->str(IDS_STRING128),3);	//cut track
			AddView(RUNTIME_CLASS(CSampleTreeTabPageManager),xml->str(IDS_STRING169),3);	//sample tree, rand tree

			m_csFileMsg = xml->str(IDS_STRING164);	//file msg
			m_csErrCreateInvFile = xml->str(IDS_STRING138);
			m_csErrMsgNoTree = xml->str(IDS_STRING181);
		}
		delete xml;
	}

	return 0;
}

void CStandTableView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,1,1,rect.right-1,rect.bottom-1);
	}
}

BOOL CStandTableView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}

	//what to do if not connected?

	return CView::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CStandTableView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CView::PreCreateWindow(cs))
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CStandTableView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
	
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());

}

LRESULT CStandTableView::OnSuiteMessage(WPARAM wParam, LPARAM lParam)
{
	switch(wParam)
	{
	case ID_NEW_ITEM:
		setupNewStandTable(TRUE);
		break;
	case ID_DELETE_ITEM:
		deleteFromStandTable();
		break;
	}

	return 0L;
}

BOOL CStandTableView::addSpecieToStandTable(void)
{
	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	CXTPDockingPaneManager *pPaneManager = NULL;
	CXTPDockingPane *pPane = NULL;
	 
	//get dcls from dialog and create standtables
	double dcls = 0.0;

	CMDIStandTableFrame *pMDIChild = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pMDIChild != NULL)
	{
		pPaneManager = pMDIChild->GetDockingPaneManager();
		if(pPaneManager != NULL)
		{
			pPane = pPaneManager->FindPane(IDC_SETTINGS_PANE);
			if(pPane != NULL)
			{
				CSettingsDataDlg *pDlg = (CSettingsDataDlg*)pPane->GetChild();
				if(pDlg != NULL)
				{
					//get dcls value
					dcls = pDlg->getAndCheckDcls();
				}
				else
				{
					//dialog not visited (created), use default value 
					dcls = /*2.0*/fDefDclsValue;
				}
			}//if(pPane != NULL)
		}//if(pPaneManager != NULL)
	}//if(pMDIChild != NULL)

	if(dcls <= 0.0)
	{
		delete dlg;
		return FALSE;
	}

	
	//get current species from first tab (cut)
	CXTPTabManagerItem *pTabItem = m_wndTabControl.getTabPage(0);
	CTabPageManager* pTabPage = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pTabItem->GetHandle()));
	int nPages = pTabPage->m_wndTabControl.getNumOfTabPages();
	for(int i=0; i<nPages;i++)
	{
		dlg->setSpeciesUsed((int)(pTabPage->m_wndTabControl.getTabPage(i))->GetData());
	}
	
	if(dlg->DoModal() == IDOK)
	{
		m_vecSpecies = dlg->getSpeciesSelected();

		if(m_vecSpecies.size() <= 0)
			return FALSE;

		int nTabPages = m_wndTabControl.getNumOfTabPages();

		for(int i = 0; i<nTabPages; i++)
		{
			CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);

			if(pItem)
			{
				if(i != TAB_SAMPLETREE)
				{
					CTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

					if(pTabManager)
					{
						if(m_vecSpecies.size() > 0)
						{
							for(UINT j=0; j<m_vecSpecies.size(); j++)
							{
								pTabManager->AddView(RUNTIME_CLASS(CSpeciesView),m_vecSpecies[j].getSpcName(),m_vecSpecies[j].getSpcID(),3);
							}
						}//if(m_vecSpecies.size() > 0)
					}//if(pManager)
				}
				else if(i == TAB_SAMPLETREE)
				{
					CSampleTreeTabPageManager *pSampleTreeTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

					if(pSampleTreeTabManager)
					{
						if(m_vecSpecies.size() > 0)
						{
							for(UINT j=0; j<m_vecSpecies.size(); j++)
							{
								pSampleTreeTabManager->AddView(RUNTIME_CLASS(CSampleTreeView),m_vecSpecies[j].getSpcName(),m_vecSpecies[j].getSpcID(),3);
							}
						}//if(m_vecSpecies.size() > 0)
					}//if(pSampleTreeManager)
				}
			}//if(pItem)
		}

		if(dcls > 0.0)
		{
			if(pPane != NULL)
			{
				CSettingsDataDlg *pDlg = (CSettingsDataDlg*)pPane->GetChild();
				if(pDlg != NULL)
				{
					//disable m_wndEdit1 in Dlg
					pDlg->setEditDisabled(TRUE);
				}
			}//if(pPane != NULL)
		}

		//create dclstables
		setupDclsTables(dcls);

	}

	delete dlg;

	return TRUE;
}


BOOL CStandTableView::setupNewStandTable(BOOL bShowMsg)
{
	if(bShowMsg && fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader();
		if(xml->Load(m_sLangFN))
		{
			if(AfxMessageBox(xml->str(IDS_STRING137),MB_OKCANCEL|MB_ICONEXCLAMATION) != IDOK)
			{
				delete xml;
				return TRUE;
			}
		}//if(xml->Load(m_sLangFN))
		delete xml;
	}//if(fileExists(m_sLangFN))


	//clear data in dialogs
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if(pFrameWnd)
	{
		CMDIStandTableFrame *pWnd = (CMDIStandTableFrame*)pFrameWnd->GetActiveFrame();
		if(pWnd)
		{
			if(pWnd->m_wndGeneralDataDlg.GetSafeHwnd())
			{
				pWnd->m_wndGeneralDataDlg.clearDlg();
			}

			if(pWnd->m_wndSettingsDataDlg.GetSafeHwnd())
			{
				pWnd->m_wndSettingsDataDlg.clearDlg();
				pWnd->m_wndSettingsDataDlg.setEditDisabled(FALSE);
			}
		}
	}


	//remove species
	int nTabPages = m_wndTabControl.getNumOfTabPages();
	for(int i=0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					int nPages = pManager->m_wndTabControl.getNumOfTabPages();
					for(int j=nPages-1; j>=0;j--)
					{
						CXTPTabManagerItem *pSelectedItem = pManager->m_wndTabControl.getTabPage(j);
						if(pSelectedItem)
						{
							pSelectedItem->Remove();
						}//if(pSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					int nPages = pSampleTreeManager->m_wndTabControl.getNumOfTabPages();
					for(int j=nPages-1; j>=0;j--)
					{
						CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.getTabPage(j);
						if(pSampleTreeSelectedItem)
						{
							pSampleTreeSelectedItem->Remove();
						}//if(pSampleTreeSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pSampleTreeManager)
			}
		}//if(pItem)
	}//for(int i=0; i<nTabPages; i++)

	//select species
	//addSpecieToStandTable();

	resetDataChanged();

	return TRUE;
}

BOOL CStandTableView::saveDataToHxlFile(void)
{
	CStringA csStr, csPart, csTmp; // ANSI output
	CString csFilePath;
	CFile cfFile;
	CString csErr1,csErr2, csErr3;

	//update data in records to get changes
	updateRecords();
	
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if(!pFrameWnd)
		return FALSE;

	CMDIStandTableFrame *pWnd = (CMDIStandTableFrame*)pFrameWnd->GetActiveFrame();
	if(!pWnd)
		return FALSE;

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csErr1 = xml->str(IDS_STRING192);
			csErr2 = xml->str(IDS_STRING193);
			csErr3 = xml->str(IDS_STRING194);
		}
		delete xml;
	}

	
			

	//Kontroll av block, enhet, delnr och sida
	//koll att delnummer ej �r 0
	int nNum;
	if(pWnd->m_wndGeneralDataDlg.getDelnr() != _T(""))
	{
		nNum = _tstoi(pWnd->m_wndGeneralDataDlg.getDelnr());
		if(nNum == 0)
		{
			pWnd->m_wndGeneralDataDlg.setDelnr(_T(""));
			pWnd->m_wndGeneralDataDlg.setUpdate(FALSE);
			AfxMessageBox(csErr3);
			return FALSE;
		}
	}

	//koll om block saknas
	if(pWnd->m_wndGeneralDataDlg.getNumber() == _T("") && (pWnd->m_wndGeneralDataDlg.getSubNumber() != _T("") || pWnd->m_wndGeneralDataDlg.getDelnr() != _T("") || pWnd->m_wndGeneralDataDlg.getSide() != _T("")))
	{
		AfxMessageBox(csErr1, MB_ICONEXCLAMATION);
		return FALSE;
	}

	//sida finns, kolla delnr
	if(pWnd->m_wndGeneralDataDlg.getSide() != _T(""))
	{
		if(pWnd->m_wndGeneralDataDlg.getDelnr() == _T(""))
		{
			if(AfxMessageBox(csErr2,MB_ICONEXCLAMATION|MB_YESNO) == IDYES)
				return FALSE;
			else
			{
				pWnd->m_wndGeneralDataDlg.setDelnr(_T("1"));
			}
		}
	}

	
	AfxGetApp()->BeginWaitCursor();

	//header
	csStr += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";

	//HXL
	csStr += "<HXL>\n";

	//Document
	csStr += "<Document>\n";

	struct tm T;
	time_t t;
	time(&t);
	localtime_s(&T, &t);
	csPart.Format("<FileDate>%04d%02d%02d</FileDate>\n",T.tm_year+1900,T.tm_mon+1,T.tm_mday);
	csStr += csPart;

	csStr += "<Type>HXL HMS</Type>\n";
	csStr += "<Ver>107</Ver>\n";
	csStr += "</Document>\n";

	//Object
	csStr += "<Object>\n";
	//objectData
	csStr += "<objectData>\n";
	//1 2 program version
	csStr += "<ProgramVersion Name=\"PROGRAMVERSION\" Var=\"1\" Type=\"2\">HXL HMS 1.0.7</ProgramVersion>\n";
	//1 3 charset
	csStr += "<Tecken Name=\"CHARSET\" Var=\"1\" Type=\"3\">ISO 8859-1</Tecken>\n";
	//2 1 stand name
	csTmp = CheckStringNotForbidden(pWnd->m_wndGeneralDataDlg.getName()) + _T(" ") + CheckStringNotForbidden(pWnd->m_wndGeneralDataDlg.getNumber());
	if(pWnd->m_wndGeneralDataDlg.getSubNumber() != _T(""))
	{
		csTmp += ":" + CheckStringNotForbidden(pWnd->m_wndGeneralDataDlg.getSubNumber());		
	}
	if(pWnd->m_wndGeneralDataDlg.getDelnr() != _T(""))
	{
		if(pWnd->m_wndGeneralDataDlg.getSubNumber() == _T(""))
			csTmp += ":";
		//Egentligen on�digt: Delnr should always be a int, in that case no check with CheckStringNotForbidden() is required.
		csTmp += "_" + CheckStringNotForbidden(pWnd->m_wndGeneralDataDlg.getDelnr());
	}
	csPart.Format("<Best Name=\"STANDID\" Var=\"2\" Type=\"1\">%s</Best>\n", csTmp);
	csStr += csPart;
	//12 4 date
	csPart.Format("<Date Name=\"DATE\" Var=\"12\" Type=\"4\">%04d%02d%02d%02d%02d%02d</Date>\n",T.tm_year+1900,T.tm_mon+1,T.tm_mday,T.tm_hour,T.tm_min,T.tm_sec);
	csStr += csPart;
	//111 1 number of species used
	int nNumOfSpc = getNumOfSpcUsed();
	if(nNumOfSpc > 0)
	{
		csPart.Format("<Numsp Name=\"NUMSPEC\" Var=\"111\" Type=\"1\">%d</Numsp>\n", nNumOfSpc);
		csStr += csPart;

		//SpecieSet
		csStr += "<SpecieSet>\n";
		csStr += "<SpecieVar Name=\"SPECIENAME\" Var=\"120\" Type=\"1\"/>\n";
		csStr += "<SpecieVar Name=\"SPECIENUMBER\" Var=\"120\" Type=\"3\"/>\n";
		csStr += "<Species>";
		//120 1 och 120 3
		csStr += getSpcNamesUsedHxl();
		csStr += "</Species>\n";
		csStr += "</SpecieSet>\n";

	}
	else
	{
		AfxMessageBox(m_csErrMsgNoTree, MB_ICONERROR);
		return FALSE;
	}

	//651 1 number of plots, in this case always 1
	csStr += "<Numplots Name=\"NUMPLOT\" Var=\"651\" Type=\"1\">1</Numplots>\n";

	//660 1 age on stand
	if(pWnd->m_wndGeneralDataDlg.getAge() != _T(""))
	{
		int age = _tstoi(pWnd->m_wndGeneralDataDlg.getAge());
		if(age >= 0)
		{
			csPart.Format("<BestAge Name=\"BESTAGE\" Var=\"660\" Type=\"1\">%d</BestAge>\n", age);
			csStr += csPart;
		}
	}

	//670 1 areal in m2
	int area = 0;
	if(pWnd->m_wndGeneralDataDlg.getArea() != _T(""))
	{
		double tarea = _tstof(pWnd->m_wndGeneralDataDlg.getArea());
		area = (int)(tarea * 10000.0);	//from ha to m2
		if(area > 0)
		{
			csPart.Format("<Areal Name=\"AREAL\" Var=\"670\" Type=\"1\">%d</Areal>\n", area);
			csStr += csPart;
		}
	}
		
	//2011 2 latitud
	if(pWnd->m_wndGeneralDataDlg.getLatitude() != _T(""))
	{
		int lat = _tstoi(pWnd->m_wndGeneralDataDlg.getLatitude());
		lat *= 10;
		if(lat >= 0)
		{
			csPart.Format("<Latitude Name=\"LATITUDE\" Var=\"2011\" Type=\"2\">%d</Latitude>\n", lat);
			csStr += csPart;
		}
	}

	//2012 1 height above sea level
	if(pWnd->m_wndGeneralDataDlg.getHeightOverSea() != _T(""))
	{
		int seahgt = _tstoi(pWnd->m_wndGeneralDataDlg.getHeightOverSea());
		if(seahgt >= 0)
		{
			csPart.Format("<HgtAboveSea Name=\"HGTABOVESEA\" Var=\"2012\" Type=\"1\">%d</HgtAboveSea>\n", seahgt);
			csStr += csPart;
		}
	}

	//2030 1 inventory method, allways total inventory (2)
	csStr += "<Metod Name=\"TAXMETHOD\" Var=\"2030\" Type=\"1\">2</Metod>\n";
	
	//2095 2 bonitet
	csTmp.Format("%s", CW2A(pWnd->m_wndGeneralDataDlg.getH100()));
	if(csTmp != _T("___"))
	{
		csTmp += _T("0");	//dm
		csPart.Format("<Bonitet2 Name=\"BONITET2\" Var=\"2095\" Type=\"2\">%s</Bonitet2>\n", csTmp);
		csStr += csPart;
	}

	//2104 1 Side 1 or 2
	if(pWnd->m_wndGeneralDataDlg.getSide() != _T(""))
	{
		int side = _tstoi(pWnd->m_wndGeneralDataDlg.getSide());
		if(side > 0)
		{
			csPart.Format("<BestSida Name=\"BESTSIDA\" Var=\"2104\" Type=\"1\">%d</BestSida>\n", side);
			csStr += csPart;
		}
	}
	
	csStr += "</objectData>\n";
	//PlotSet
	csStr += "<PlotSet>\n";
	//Plot
	csStr += "<Plot>\n";
	//2052 1 plot id
	csPart.Format("<Id Name=\"ID\" Var=\"2052\" Type=\"1\">0</Id>\n");
	csStr += csPart;
	//671 1 areal
	if(area > 0)
	{
		csPart.Format("<Areal Name=\"AREAL\" Var=\"671\" Type=\"1\">%d</Areal>\n", area);
		csStr += csPart;
	}
	
	//End of Plot
	csStr += "</Plot>\n";
	csStr += "</PlotSet>\n";


	//TreeSet
	if(nNumOfSpc > 0)
	{
		csStr += "<TreeSet>\n";
		csStr += "<TreeVar Name=\"SPECNR\" Var=\"652\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"DBH\" Var=\"653\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"HGTNUM\" Var=\"654\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"HGT\" Var=\"656\" Type=\"1\" Def=\"50\"/>\n";
		csStr += "<TreeVar Name=\"HGT2\" Var=\"656\" Type=\"1\" Def=\"51\"/>\n";
		csStr += "<TreeVar Name=\"BARK\" Var=\"2003\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"AGE\" Var=\"2001\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"TYPE\" Var=\"2004\" Type=\"2\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"BONITET\" Var=\"2006\" Type=\"1\" Def=\"\"/>\n";
		csStr += "<TreeVar Name=\"PLOT\" Var=\"2052\" Type=\"1\" Def=\"\"/>\n";

		//Tree
		csStr += "<Tree>";
		//652 1, 653 1, 654 1, 656 1 50, 656 1 51, 2003 2, 2001 2, 2004 2, 2006 1, 2052 1
		csPart = getTreeDataHxl();
		if(csPart == _T("FALSE"))
			return FALSE;

		csStr += csPart;

		csStr += "</Tree>\n";
		csStr += "</TreeSet>\n";
	}

	csStr += "</Object>\n";
	csStr += "</HXL>\n";

	//create file
	if(CreateFile(csFilePath, _T(".hxl")) == FALSE)
	{
		return FALSE;
	}

	//open file
	if(cfFile.Open(csFilePath, CFile::modeReadWrite) == 0)
	{
		//can't open file
		return FALSE;
	}

	
	cfFile.Write(csStr, csStr.GetLength());
	cfFile.Flush();
	cfFile.Close();

	resetDataChanged();

	AfxGetApp()->EndWaitCursor();

	CString msg;
	msg = m_csFileMsg + _T("\r\n");
	msg += csFilePath;
	AfxMessageBox(msg, MB_OK | MB_ICONINFORMATION);
	return TRUE;
}

CString CStandTableView::getTreeDataHxl()
{
	CString csTemp, csTot;
	CString csMsgStr;

	int spc, dbh, hgt, hgt2, hgtnum, bark, age, bonitet, type, plot;
	int  res;

	BOOL m_bSampleTree = getSampleTrees();
	if(m_bSampleTree == FALSE)
		return _T("FALSE");

	CMyTransactionSampleTree m_SampleTree;

	csTot = _T("");
	for(int i=0; i<m_wndTabControl.getNumOfTabPages(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(!pItem)
			return _T("");

		if(i != TAB_SAMPLETREE)	//not sampletrees
		{
			CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
			if(!pManager)
				return _T("");

			for(int j=0; j<pManager->m_wndTabControl.getNumOfTabPages(); j++)	//for every specie on each tab
			{
				CXTPTabManagerItem *pSpcItem = pManager->m_wndTabControl.getTabPage(j);
				if(!pSpcItem)
					return _T("");
				
				CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpcItem->GetHandle()));
				if(!pSpcView)
					return _T("");

				CXTPReportRecords *pRecs = pSpcView->m_wndReport.GetRecords();
				if(!pRecs)
					return _T("");

				for(int k=0; k<pRecs->GetCount(); k++)
				{
					CSpeciesViewReportRec *pSpcRec = (CSpeciesViewReportRec*)pRecs->GetAt(k);
					if(!pSpcRec)
						return _T("");

					int nSize = (int)pSpcRec->getColumnFloat(2);
					
					if(nSize > 0)
					{
						for(int m=0; m<nSize; m++)
						{
							spc = dbh = hgt = hgt2 = hgtnum = bark = age = bonitet = type = plot = 0;

							//652 1 specie code
							spc = (int)pSpcItem->GetData();

							//check if there is sample trees in m_vecSampleTrees that belongs to a diameter class
							// if it is, isSampleTreeSet removes tree from m_vecSampleTrees
							if(isSampleTreeSet(pSpcItem->GetData(), pSpcRec->getColumnFloat(0), pSpcRec->getColumnFloat(1), i, m_SampleTree) == FALSE)
							{
								
								//653 1 dbh
								dbh = (int)((pSpcRec->getColumnFloat(0)*10.0 + pSpcRec->getColumnFloat(1)*10.0)/2);
								
								//654 1 num of heights per tree
								hgtnum = 0;

								//2001 1 tree age = 0 if not sample tree
								age = 0;

								//2003 2 single bark thickness (mm) = 0 if not sample tree
								bark = 0;

								//2004 2 tree type
								if(i == TAB_CUT)
								{
									type = 0;
								}
								else if(i == TAB_STANDING)
								{
									type = 1;
								}
								else if(i == TAB_CUT_TRACK)
								{
									type = 2;
								}
							}
							else
							{
								//653 1 dbh
								dbh = (int)m_SampleTree.getDbh();

								//654 1 num of heights per tree
								//655 1 height codes
								//656 1 heights (cm)
								hgtnum = 0;

								if(m_SampleTree.getHgt() > 0.0)
								{
									hgtnum++;
									hgt = (int)(m_SampleTree.getHgt()*10.0);
								}

								if(m_SampleTree.getGCrown() > 0.0)
								{
									hgtnum++;
									hgt2 = (int)(m_SampleTree.getHgt()*10.0*(1.0-m_SampleTree.getGCrown()/100.0));
								}

								//2001 1 tree age
								age = (int)m_SampleTree.getAge();
								
								//2003 2 single bark thickness (mm) = 0 if not sample tree
								bark = (int)m_SampleTree.getBrk();
								
								//2004 2 tree type
								 type = m_SampleTree.getType();

								 //2006 1 bonitet H100/H50 dm
								 bonitet = (int)m_SampleTree.getBonitet() * 10;	//dm
							}
							//652 1, 653 1, 654 1, 656 1 50, 656 1 51, 2003 2, 2001 2, 2004 2, 2006 1, 2052 1
							plot = 0;	//plot allways 0
							csTemp.Format(_T("%d,%d,%d,%d,%d,%d,%d,%d,%d, %d\r\n"),
								spc, dbh, hgtnum, hgt, hgt2, bark, age, type, bonitet, plot);
							csTot += csTemp;
						}//for(m)
					}//if(pSpcRec->getColumnFloat(2) > 0.0)
				}//for(k)
			}//for(j)
		}//if(i != TAB_SAMPLETREE)
	}//for(i)

	//Check if Rand Trees exists, don't match rand trees into any diameter class
	if(m_vecSampleTrees.size() > 0)
	{
		for(int i = (int)m_vecSampleTrees.size(); i>0; i--)
	{
			m_SampleTree = m_vecSampleTrees[i-1];
			spc = dbh = hgt = hgt2 = hgtnum = bark = age = bonitet = type = plot = 0;

			//652 1 specie code
			spc = (int)m_SampleTree.getSpcId();

			//653 1 dbh
			dbh = (int)m_SampleTree.getDbh();

			//654 1 num of heights per tree
			//655 1 height codes
			//656 1 heights (cm)

			if(m_SampleTree.getHgt() > 0.0)
			{
				hgtnum++;

				hgt = (int)(m_SampleTree.getHgt()*10.0);
			}

			if(m_SampleTree.getGCrown() > 0.0)
			{
				hgtnum++;
				hgt2 = (int)(m_SampleTree.getHgt()*10.0*(1.0-m_SampleTree.getGCrown()/100.0));
			}


			//2001 1 tree age
			age = (int)m_SampleTree.getAge();

			//2003 2 single bark thickness (mm) = 0 if not sample tree
			bark = (int)m_SampleTree.getBrk();

			//2004 2 tree type
			type = (int)m_SampleTree.getType();

			//2006 1 bonitet H100/H50 dm
			bonitet = (int)m_SampleTree.getBonitet() * 10; //dm

			if(type == 3)	//Rand Tree
			{
				//erase sample tree from vector
				m_vecSampleTrees.erase(m_vecSampleTrees.begin() + i-1);
				
				//652 1, 653 1, 654 1, 656 1 50, 656 1 51, 2003 2, 2001 2, 2004 2, 2006 1, 2052 1
				plot = 0;	//plot allways 0
				csTemp.Format(_T("%d,%d,%d,%d,%d,%d,%d,%d,%d, %d\r\n"),
					spc, dbh, hgtnum, hgt, hgt2, bark, age, type, bonitet, plot);
				csTot += csTemp;
			}

		}//for(int i =m_vecSampleTrees.size(); i>0; i--)

	}
		

	//sample trees that doesn't belong to any diameter class
	if(m_vecSampleTrees.size() > 0)
	{
		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				csMsgStr = xml->str(IDS_STRING155);
			}
			delete xml;
		}

		if((res = AfxMessageBox(csMsgStr, MB_YESNO|MB_ICONEXCLAMATION)) == IDYES)
		{
			while(m_vecSampleTrees.size() > 0)
			{
				m_SampleTree = m_vecSampleTrees.back();
				spc = dbh = hgt = hgt2 = hgtnum = bark = age = bonitet = type = plot = 0;

				//652 1 specie code
				spc = (int)m_SampleTree.getSpcId();

				//653 1 dbh
				dbh = (int)m_SampleTree.getDbh();

				//654 1 num of heights per tree
				//655 1 height codes
				//656 1 heights (cm)
				
				if(m_SampleTree.getHgt() > 0.0)
				{
					hgtnum++;
					
					hgt = (int)(m_SampleTree.getHgt()*10.0);
				}

				if(m_SampleTree.getGCrown() > 0.0)
				{
					hgtnum++;
					hgt2 = (int)(m_SampleTree.getHgt()*10.0*(1.0-m_SampleTree.getGCrown()/100.0));
				}

				
				//2001 1 tree age
				age = (int)m_SampleTree.getAge();

				//2003 2 single bark thickness (mm) = 0 if not sample tree
				bark = (int)m_SampleTree.getBrk();

				//2004 2 tree type
				type = (int)m_SampleTree.getType();

				//2006 1 bonitet H100/H50 dm
				bonitet = (int)m_SampleTree.getBonitet() * 10; //dm

				//erase sample tree from vector
				m_vecSampleTrees.pop_back();

				//652 1, 653 1, 654 1, 656 1 50, 656 1 51, 2003 2, 2001 2, 2004 2, 2006 1, 2052 1
				plot = 0;	//plot allways 0
				csTemp.Format(_T("%d,%d,%d,%d,%d,%d,%d,%d,%d, %d\r\n"),
					spc, dbh, hgtnum, hgt, hgt2, bark, age, type, bonitet, plot);
				csTot += csTemp;

			}//for(int i =m_vecSampleTrees.size(); i>0; i--)
		}//if(AfxMessageBox

	}//if(m_vecSampleTrees.size() > 0)

	return csTot;
}


BOOL CStandTableView::isSampleTreeSet(double id, double start, double stop, int type, CMyTransactionSampleTree &stree)
{
	for(UINT i=0; i<m_vecSampleTrees.size(); i++)
	{
		if((m_vecSampleTrees[i].getDbh() >= start*10.0) && (m_vecSampleTrees[i].getDbh() < stop*10.0) &&
			(m_vecSampleTrees[i].getType() == type) && (m_vecSampleTrees[i].getSpcId() == id))
		{
			stree = m_vecSampleTrees[i];
			m_vecSampleTrees.erase(m_vecSampleTrees.begin() + i);
			return TRUE;
		}
	}

	return FALSE;
}


BOOL CStandTableView::getSampleTrees()
{
	m_vecSampleTrees.clear();

	CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(TAB_SAMPLETREE);
	if(!pItem)
		return FALSE;
	
	CSampleTreeTabPageManager *pManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
	if(!pManager)
		return FALSE;

	return pManager->getSampleTrees(m_vecSampleTrees);
}

int CStandTableView::getNumOfSpcUsed()
{
	int nNumOfSpc = 0;

	m_vecSpeciesUsed.clear();

	//get num of species from first tab (cut)
	CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
	int nItemSize = m_wndTabControl.getNumOfTabPages();

	if(!pItem)
		return 0;
	CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
	if(!pManager)
		return 0;

	//loop thru all species
	for(int i=0; i<pManager->m_wndTabControl.getNumOfTabPages(); i++)	
	{
		//check tabs for each specie
		for(int j=0; j<nItemSize; j++)
		{
			if(j != TAB_SAMPLETREE)
			{
				CXTPTabManagerItem *pTabItem = m_wndTabControl.getTabPage(j);
				if(!pTabItem)
					return 0;

				CTabPageManager* pTabView = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pTabItem->GetHandle()));
				if(!pTabView)
					return 0;

				CXTPTabManagerItem *pSpcItem = pTabView->m_wndTabControl.getTabPage(i);
				if(!pSpcItem)
					return 0;

				CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpcItem->GetHandle()));
				if(!pSpcView)
					return 0;

				if(pSpcView->getNumOfTrees() > 0)
				{
					nNumOfSpc++;
					m_vecSpeciesUsed.push_back((int)(pTabView->m_wndTabControl.getTabPage(i))->GetData());
					break;
				}
			}//if(j != TAB_SAMPLETREE)
			else if(j == TAB_SAMPLETREE)
			{
				CXTPTabManagerItem *pSampleTabItem = m_wndTabControl.getTabPage(j);
				if(!pSampleTabItem)
					return 0;

				CSampleTreeTabPageManager *pSampleTabView = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pSampleTabItem->GetHandle()));
				if(!pSampleTabView)
					return 0;

				CXTPTabManagerItem *pSampleSpcItem = pSampleTabView->m_wndTabControl.getTabPage(i);
				if(!pSampleSpcItem)
					return 0;

				CSampleTreeView *pSampleSpcView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pSampleSpcItem->GetHandle()));
				if(!pSampleSpcView)
					return 0;

				if(pSampleSpcView->getNumOfTrees() > 0)
				{
					nNumOfSpc++;
					m_vecSpeciesUsed.push_back((int)(pSampleTabView->m_wndTabControl.getTabPage(i))->GetData());
					break;
				}
			}//else if(j == TAB_SAMPLETREE)
		}//for(j)
	}//for(i)

	return nNumOfSpc;
}

int CStandTableView::getNumOfSpc()
{
	int nNumOfSpc = 0;

	//get num of species from first tab (cut)
	CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
	
	if(!pItem)
		return 0;
	CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
	if(!pManager)
		return 0;

	return pManager->m_wndTabControl.getNumOfTabPages();
}


CString CStandTableView::getSpcNamesUsedHxl()
{
	CString csRes = _T(""), csPart = _T("");

	for(UINT i=0; i<m_vecSpeciesUsed.size(); i++)
	{
		csPart.Format(_T("%s,%d\r\n"),getSpcName(m_vecSpecies,m_vecSpeciesUsed[i]),m_vecSpeciesUsed[i]);
		csRes += csPart;
	}

	return csRes;
}


int CStandTableView::getNumOfTrees()
{
	int nNumOfSpc = 0;
	int nTot = 0;

	if(m_wndTabControl.GetSafeHwnd() == NULL)
		return 0;
	
	int nItemSize = m_wndTabControl.getNumOfTabPages();

	if(nItemSize <= 0)
		return 0;

	//get num of species from first tab (cut)
	CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
	
	if(!pItem)
		return 0;
	CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
	if(!pManager)
		return 0;

	//loop thru all species
	for(int i=0; i<pManager->m_wndTabControl.getNumOfTabPages(); i++)	
	{
		//check tabs for each specie
		for(int j=0; j<nItemSize; j++)
		{
			if(j != TAB_SAMPLETREE)
			{
				CXTPTabManagerItem *pTabItem = m_wndTabControl.getTabPage(j);
				if(!pTabItem)
					return 0;

				CTabPageManager* pTabView = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pTabItem->GetHandle()));
				if(!pTabView)
					return 0;

				CXTPTabManagerItem *pSpcItem = pTabView->m_wndTabControl.getTabPage(i);
				if(!pSpcItem)
					return 0;

				CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpcItem->GetHandle()));
				if(!pSpcView)
					return 0;

				nNumOfSpc = pSpcView->getNumOfTrees();
				if(nNumOfSpc > 0)
				{
					nTot += nNumOfSpc;
				}
			}//if(j != TAB_SAMPLETREE)
			else if(j == TAB_SAMPLETREE)
			{
				CXTPTabManagerItem *pSampleTabItem = m_wndTabControl.getTabPage(j);
				if(!pSampleTabItem)
					return 0;

				CSampleTreeTabPageManager *pSampleTabView = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pSampleTabItem->GetHandle()));
				if(!pSampleTabView)
					return 0;

				CXTPTabManagerItem *pSampleSpcItem = pSampleTabView->m_wndTabControl.getTabPage(i);
				if(!pSampleSpcItem)
					return 0;

				CSampleTreeView *pSampleSpcView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pSampleSpcItem->GetHandle()));
				if(!pSampleSpcView)
					return 0;

				nNumOfSpc = pSampleSpcView->getNumOfTrees();
				if(nNumOfSpc > 0)
				{
					nTot += nNumOfSpc;
				}
			}//else if(j == TAB_SAMPLETREE)
		}//for(j)
	}//for(i)

	return nTot;
}

BOOL CStandTableView::CreateFile(CString &str, LPCTSTR fileext)
{
	CString csDirPath, csFileName, csTemp;
	CString csName,csNumber,csSubNumber;
	CString csErr1,csErr2,csErr3, csErr4, csErr5, csErr6;
	CString csDelnr, csSida;

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csErr1 = xml->str(IDS_STRING162);
			csErr2 = xml->str(IDS_STRING163);
			csErr4 = xml->str(IDS_STRING165);
			csErr5 = xml->str(IDS_STRING166);
		}
		delete xml;
	}

	//check if library exists, create if not
	/*
	if(isLibraryCreated(csDirPath) == FALSE)	//returns path to directory
		return FALSE;*/
	//HMS-81 Tar alltid s�kv�gen till user/minadokument/HMS/Data
	TCHAR szPath[MAX_PATH]=_T("");
	if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szPath)))
		csDirPath.Format(_T("%s\\%s%s"), szPath, SUBDIR_HMS,SUBDIR_DATA);	
	else
		csDirPath=_T("");


	//create file
	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if(!pFrameWnd)
		return FALSE;
	CMDIStandTableFrame *pWnd = (CMDIStandTableFrame*)pFrameWnd->GetActiveFrame();
	if(!pWnd)
		return FALSE;

	//csFileName = csDirPath + _T("\\");
	//csDirPath += _T("\\");

	csName = pWnd->m_wndGeneralDataDlg.getName();
	csNumber = pWnd->m_wndGeneralDataDlg.getNumber();
	csSubNumber = pWnd->m_wndGeneralDataDlg.getSubNumber();

	csDelnr = pWnd->m_wndGeneralDataDlg.getDelnr();
	csSida = pWnd->m_wndGeneralDataDlg.getSide();

	//check that property name has been entered
	if(csName == _T(""))// && csNumber == _T("") && csSubNumber == _T(""))
	{
		AfxMessageBox(csErr4);
		return FALSE;
	}

	
	if(csName != _T(""))
		csFileName += csName;
	if(csNumber != _T(""))
		csFileName += _T(" ") + csNumber;
	if(csSubNumber != _T(""))
	{
		csFileName += _T(";") + csSubNumber;
	}

	if(csDelnr != _T(""))
	{
		if(csSubNumber == _T(""))
		{
			csFileName += _T(";");
		}
		csFileName += _T("-") + csDelnr;

		if(csSida != _T(""))
		{
			csFileName += _T("-") + csSida;
		}
	}


	csTemp.Format(_T("%s"), fileext);
	csFileName += csTemp;

	if(csFileName.Find(_T("\\")) >= 0 || csFileName.Find(_T("/")) >= 0 || csFileName.Find(_T(":")) >= 0 || csFileName.Find(_T("'")) >= 0 || csFileName.Find(_T("*")) >= 0 || csFileName.Find(_T("?")) >= 0 || csFileName.Find(_T("\"")) >= 0 || csFileName.Find(_T("<")) >= 0 || csFileName.Find(_T(">")) >= 0 || csFileName.Find(_T("|")) >= 0)
	{
		//felaktigt tecken i filnamn
		csErr6 = csErr5 + _T("\\ / : ' * ? \" < > |");
		AfxMessageBox(csErr6);
		return FALSE;
	}

	// show a filsave-dialog
	TCHAR tzFilename[1024], tzInitDir[1024];	//, tzTitle[50];
	_stprintf(tzFilename, csFileName);
	_stprintf(tzInitDir, csDirPath);
	
	CFileDialog dlgF(FALSE);
	dlgF.m_ofn.lpstrFilter = (LPTSTR)_T("HXL (*.hxl)\0*.hxl\0\0");
	dlgF.m_ofn.hwndOwner = AfxGetMainWnd()->GetSafeHwnd();
	dlgF.m_ofn.lpstrFile = tzFilename;
	dlgF.m_ofn.lpstrInitialDir = tzInitDir;
	dlgF.m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT;
	dlgF.m_ofn.lpstrDefExt = _T(".hxl");

	if(dlgF.DoModal() != IDOK)
	{
		return FALSE;
	}

	csFileName = dlgF.GetPathName();
	
	CFile cfFile;

	
	//create file
	if(cfFile.Open(csFileName, CFile::modeReadWrite|CFile::modeCreate) == 0)
	{
		//can't create file
		AfxMessageBox(m_csErrCreateInvFile);
		return FALSE;
	}

	cfFile.Close();
	

	str = csFileName;

	return TRUE;
}

BOOL CStandTableView::isLibraryCreated(CString &str)
{
	
	CString csPath,csData;
	TCHAR szPath[MAX_PATH];
	if(SUCCEEDED(SHGetFolderPath(NULL,CSIDL_PERSONAL|CSIDL_FLAG_CREATE,NULL,0,szPath)))
		csPath.Format(_T("%s\\%s"),szPath,SUBDIR_HMS);
	else
		return FALSE;

	BOOL m_bRes = TRUE;
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csData = xml->str(IDS_STRING161);
		}
		else
			m_bRes = FALSE;
		delete xml;
	}
	else
		m_bRes = FALSE;

	if(m_bRes == FALSE)
	{
		csData = SUBDIR_DATA;
	}

	//create C:\Documents and settings\current user\My documents\HMS directory if not allready created
	if(!CreateDirectory(csPath,NULL))
		if(GetLastError() != ERROR_ALREADY_EXISTS)
			return FALSE;

	csPath += csData;
	//create C:\Documents and settings\current user\My documents\HMS\Data directory if not allready created
	if(!CreateDirectory(csPath,NULL))
		if(GetLastError() != ERROR_ALREADY_EXISTS)
			return FALSE;

	str = csPath;

	return TRUE;
}

void CStandTableView::updateRecords()
{
	for(int i=0; i<m_wndTabControl.getNumOfTabPages(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					for(int j=0; j<pManager->m_wndTabControl.getNumOfTabPages(); j++)
					{
						CXTPTabManagerItem *pSpcItem = pManager->m_wndTabControl.getTabPage(j);
						if(pSpcItem)
						{
							CSpeciesView* pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pSpcItem->GetHandle()));
							if(pSpcView)
							{
								pSpcView->updateRecord();
							}//if(pSpcView)
						}//if(pSpcItem)
					}//for(j)
				}//if(pManager)
			}
			else
			{
				CSampleTreeTabPageManager *pManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					for(int j=0; j<pManager->m_wndTabControl.getNumOfTabPages(); j++)
					{
						CXTPTabManagerItem *pSpcItem = pManager->m_wndTabControl.getTabPage(j);
						if(pSpcItem)
						{
							CSampleTreeView* pSpcView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pSpcItem->GetHandle()));
							if(pSpcView)
							{
								pSpcView->updateRecord();
							}//if(pSpcView)
						}//if(pSpcItem)
					}//for(j)
				}//if(pManager)
			}
		}//if(pItem)
	}//for(i)
}



int CStandTableView::removeSpecieFromStandTable()
{
	int nId;
	CXTPTabManagerItem *pActTab = m_wndTabControl.getSelectedTabPage();

	if(pActTab)
	{
		if(pActTab->GetIndex() != TAB_SAMPLETREE)
		{
			CTabPageManager *pActManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pActTab->GetHandle()));
			if(pActManager)
			{
				if(pActManager->m_wndTabControl.getNumOfTabPages() <= 0)
					return 0;//TRUE;
				nId = (int)(pActManager->m_wndTabControl.getSelectedTabPage())->GetData();
			}//if(pActManager)
		}
		else if(pActTab->GetIndex() == TAB_SAMPLETREE)
		{
			CSampleTreeTabPageManager *pSampleTreeActManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pActTab->GetHandle()));
			if(pSampleTreeActManager)
			{
				if(pSampleTreeActManager->m_wndTabControl.getNumOfTabPages() <= 0)
					return 0;//TRUE;
				nId = (int)(pSampleTreeActManager->m_wndTabControl.getSelectedTabPage())->GetData();
			}//if(pSampleTreeActManager)
		}
	}

	int nTabPages = m_wndTabControl.getNumOfTabPages();

	if(nTabPages <= 0)
		return 0;//TRUE;

	CString csDelMsg;
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csDelMsg = xml->str(IDS_STRING154);
		}
		delete xml;
	}

	//ask if selected specie should be deleted
	if(AfxMessageBox(csDelMsg, MB_YESNO|MB_ICONEXCLAMATION) != IDYES)
	{
		return 1;//TRUE;
	}

	int nPages = 0;

	for(int i=0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					nPages = pManager->m_wndTabControl.getNumOfTabPages();
					for(int j=0; j<nPages;j++)
					{
						CXTPTabManagerItem *pSelectedItem = pManager->m_wndTabControl.getTabPage(j);
						if(pSelectedItem)
						{
							if(pSelectedItem->GetData() == nId)
							{
								pSelectedItem->Remove();
							}
						}//if(pSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					nPages = pSampleTreeManager->m_wndTabControl.getNumOfTabPages();
					for(int j=0; j<nPages;j++)
					{
						CXTPTabManagerItem *pSampleTreeSelectedItem = pSampleTreeManager->m_wndTabControl.getTabPage(j);
						if(pSampleTreeSelectedItem)
						{
							if(pSampleTreeSelectedItem->GetData() == nId)
							{
								pSampleTreeSelectedItem->Remove();
							}
						}//if(pSampleTreeSelectedItem)
					}//for(int j=0; j<nPages;j++)
				}//if(pSampleTreeManager)
			}
		}//if(pItem)
	}//for(int i=0; i<nTabPages; i++)

	return (nPages - 1);;//TRUE;
}




BOOL CStandTableView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( _T("Out of memory creating a view.\n") );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( _T("Warning: couldn't create client tab for view.\n") );
		// pWnd will be cleaned up by PostNcDestroy
		AfxMessageBox(_T("Failed to create Window!"));
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CStandTableView::setupDclsTables(double dcls)
{
	int nTabPages = m_wndTabControl.getNumOfTabPages();
	for(int i=0; i<nTabPages; i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					pManager->setupDclsTables(dcls);
				}//if(pManager)
			}
			else if(i == TAB_SAMPLETREE)
			{
				//creates empty row when initialized
			/*	CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
				if(pSampleTreeManager)
				{
					pSampleTreeManager->setupDclsTables(dcls);
				}//if(pSampleTreeManager)
				*/
			}
		}//if(pItem)
	}//for(int i=0; i<nTabPages; i++)
}

void CStandTableView::deleteFromStandTable(void)
{
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if(pItem)
	{
		int index = pItem->GetIndex();
		if(index != TAB_SAMPLETREE)
		{
			CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
			if(pManager)
			{
				pManager->deleteFromStandTable();
			}//if(pManager)
		}
		else if(index == TAB_SAMPLETREE)
		{
			//creates empty row when initialized
			CSampleTreeTabPageManager *pSampleTreeManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
			if(pSampleTreeManager)
			{
				pSampleTreeManager->deleteSampleTree();
			}//if(pSampleTreeManager)
		}
	}//if(pItem)

}


BOOL CStandTableView::isOkToClose()
{
	for(int i=0; i<m_wndTabControl.getNumOfTabPages(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					if(pManager->isOkToClose() == FALSE)
						return FALSE;
				}//if(pManager)
			}//if(i != TAB_SAMPLETREE)
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pTabManager)
				{
					if(pTabManager->isOkToClose() == FALSE)
						return FALSE;
				}//if(pManager)
			}//else if(i == TAB_SAMPLETREE)
		}//if(pItem)
	}//for(i)

	//check GeneralDataDlg
	CMDIStandTableFrame *pMDIChild = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pMDIChild)
	{
		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if(pPaneManager)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_GENERAL_DATA_PANE);
			if(pPane)
			{
				CGeneralDataDlg *pDlg = (CGeneralDataDlg*)pPane->GetChild();
				if(pDlg)
				{
					if(pDlg->isOkToClose() == FALSE)
						return FALSE;
				}//if(pDlg)
			}//if(pPane)
		}//if(pPaneManager)
	}//if(pMDIChild)

	return TRUE;
}

void CStandTableView::resetDataChanged()
{
	//set m_bDataNotChanged = TRUE for all tabs
	for(int i=0; i<m_wndTabControl.getNumOfTabPages(); i++)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(i);
		if(pItem)
		{
			if(i != TAB_SAMPLETREE)
			{
				CTabPageManager *pManager = DYNAMIC_DOWNCAST(CTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pManager)
				{
					pManager->resetDataChanged();
				}//if(pManager)
			}//if(i != TAB_SAMPLETREE)
			else if(i == TAB_SAMPLETREE)
			{
				CSampleTreeTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager, CWnd::FromHandle(pItem->GetHandle()));
				if(pTabManager)
				{
					pTabManager->resetDataChanged();
				}//if(pManager)
			}//else if(i == TAB_SAMPLETREE)
		}//if(pItem)
	}//for(i)

	//reset for dialogs
	CMDIStandTableFrame *pMDIChild = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pMDIChild)
	{
		CXTPDockingPaneManager *pPaneManager = pMDIChild->GetDockingPaneManager();
		if(pPaneManager)
		{
			CXTPDockingPane *pPane = pPaneManager->FindPane(IDC_GENERAL_DATA_PANE);
			if(pPane)
			{
				CGeneralDataDlg *pDlg = (CGeneralDataDlg*)pPane->GetChild();
				if(pDlg)
				{
					pDlg->resetDataChanged();
				}//if(pDlg)
			}//if(pPane)
		}//if(pPaneManager)
	}//if(pMDIChild)
}

BOOL CStandTableView::openInvHxlFile()
{
	//TODO: kolla ifall data finns som ej �r sparat, varna isf
	//�ppna fildialog, anv�nd Peters preview-dialog
	//l�s in filen
	//ta hand om all data
	//m�ste kolla inventeringstyp, endast totalinventering
	//vart ska datat skrivas ner i vyerna?

	BOOL bRet = TRUE;

	setupNewStandTable(FALSE);

	COpenFileFunctions *dlg = new COpenFileFunctions();

	if(dlg->readDataFromFile()== FALSE)
	{
		bRet = FALSE;
	}

	delete dlg;

	return bRet;
}

