// UMStandTable.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "Resource.h"
#include "MDIStandTableFrame.h"
#include "StandTableView.h"
#include "PrlViewAndPrint.h"

static AFX_EXTENSION_MODULE UMStandTableDLL = { NULL, NULL };

//include additional dependency "Version.lib" under linker/input in properties

HINSTANCE hInst = NULL;	

extern "C" void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo);

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInst = hInstance;
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMStandTable.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMStandTableDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMStandTable.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMStandTableDLL);
	}
	return 1;   // ok
}

void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMStandTableDLL);

	CString csLangFN;
	CString csVersion;
	CString csCopyright;
	CString csCompany;

	csLangFN.Format(_T("%s%s"),getLanguageDir(), PROGRAM_NAME);

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
		RUNTIME_CLASS(CMDIFrameDoc),	/*doc*/
		RUNTIME_CLASS(CMDIStandTableFrame),	/*frame*/
		RUNTIME_CLASS(CStandTableView)));	/*view*/
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW, suite, csLangFN, TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3,
			RUNTIME_CLASS(CMDIPrlViewAndPrintDoc),
			RUNTIME_CLASS(CMDIPrlViewAndPrintFrame),
			RUNTIME_CLASS(CPrlViewAndPrint)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW3,suite,csLangFN,TRUE));

	csVersion = getVersionInfo(hInst, VER_NUMBER);
	csCopyright = getVersionInfo(hInst, VER_COPYRIGHT);
	csCompany = getVersionInfo(hInst, VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2/* Set to 1 to indicate a SUITE; 2 indicates a  User Module*/,
		csLangFN.GetBuffer(),
		csVersion.GetBuffer(),
		csCopyright.GetBuffer(),
		csCompany.GetBuffer()));
}


