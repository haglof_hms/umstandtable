// SampleTreeView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SampleTreeView.h"
#include "SampleTreeViewReportRec.h"
#include "StandTableView.h"
#include "SampleTreeTabpageManager.h"
#include "MDIStandTableFrame.h"
#include "NumOfCopiesDlg.h"



// CSampleTreeView

IMPLEMENT_DYNCREATE(CSampleTreeView, CXTResizeFormView)

CSampleTreeView::CSampleTreeView()
	: CXTResizeFormView(CSampleTreeView::IDD)
{
	m_bPopulated = FALSE;
	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);
	m_bDataNotChanged = TRUE;

}

CSampleTreeView::~CSampleTreeView()
{
	csDefaultType = _T("");
}

void CSampleTreeView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CSampleTreeView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOUSEACTIVATE()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, IDC_REPORT, OnValueChanged)
	ON_NOTIFY(NM_RCLICK, IDC_REPORT, OnReportItemRClick)
END_MESSAGE_MAP()


// CSampleTreeView diagnostics

#ifdef _DEBUG
void CSampleTreeView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSampleTreeView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSampleTreeView message handlers
BOOL CSampleTreeView::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CSampleTreeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	
	if(m_wndReport.GetSafeHwnd() == NULL)
	{
		//create sheet
		if(!m_wndReport.Create(WS_CHILD | WS_TABSTOP | WS_VISIBLE | WM_VSCROLL|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, IDC_REPORT))
		{
			return -1;
		}
	}
	
	return 0;
}

void CSampleTreeView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	RECT rect;
	GetClientRect(&rect);
	if(m_wndReport.GetSafeHwnd() != NULL)
	{
		setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);
	}

}



int CSampleTreeView::OnMouseActivate(CWnd *pDesktopWnd, UINT nHitTest, UINT message)
{
	return CWnd::OnMouseActivate(pDesktopWnd, nHitTest, message);
}

void CSampleTreeView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	CXTPReportColumn *pCol = NULL;

	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			if(m_wndReport.GetSafeHwnd() != NULL)
			{

				m_sarrTreeTypes.RemoveAll();
				m_sarrTreeTypes.Add(xml->str(IDS_STRING126));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING127));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING128));
				m_sarrTreeTypes.Add(xml->str(IDS_STRING168));
				csDefaultType = xml->str(IDS_STRING126);

				m_csErrMsgSampleTree = xml->str(IDS_STRING182);
				m_csErrMsgRandTree = xml->str(IDS_STRING183);

				m_csMenuCopy1 = xml->str(IDS_STRING184);
				m_csMenuCopy2 = xml->str(IDS_STRING185);
				
				m_wndReport.ShowWindow(SW_NORMAL);
				
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_DBH, xml->str(IDS_STRING143), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_HGT, xml->str(IDS_STRING144), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_GCROWN, xml->str(IDS_STRING145), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_BARK, xml->str(IDS_STRING146), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_AGE, xml->str(IDS_STRING147), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);


				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_BONITET, xml->str(IDS_STRING148), 100));
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;	//select all text when click on item
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER | ES_CENTER;	//only allow numbers
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(ST_COL_TYPE, xml->str(IDS_STRING152), 100));
				pCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(0),0);
				pCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(1),1);
				pCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(2),2);
				pCol->GetEditOptions()->AddConstraint(m_sarrTreeTypes.GetAt(3),3);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();
				pCol->SetHeaderAlignment(DT_CENTER);
				pCol->SetAlignment(DT_CENTER);


				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.GetReportHeader()->AllowColumnReorder(TRUE);
				m_wndReport.GetReportHeader()->AllowColumnResize(TRUE);
				m_wndReport.GetReportHeader()->SetAutoColumnSizing(TRUE);
				m_wndReport.SetMultipleSelection(TRUE);//FALSE);
				m_wndReport.SetGridStyle(TRUE, xtpReportGridSolid);
				m_wndReport.SetGridStyle(FALSE, xtpReportGridSolid);	
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.GetReportHeader()->AllowColumnSort(FALSE);	//set if sortable

				COLORREF clr = m_wndReport.GetPaintManager()->m_clrHeaderControl.GetStandardColor();
				m_wndReport.GetPaintManager()->m_clrHighlight.SetCustomValue(clr);
				m_wndReport.GetPaintManager()->m_clrHighlightText.SetCustomValue(BLACK);
				
				RECT rect;
				GetClientRect(&rect);
				setResize(&m_wndReport,1,1,rect.right-1,rect.bottom-1);

				m_wndReport.UpdateWindow();

				//create empty row
				m_wndReport.AddRecord(new CSampleTreeViewReportRec(csDefaultType));
				m_bPopulated = TRUE;
				m_wndReport.Populate();
				m_wndReport.UpdateWindow();
				
			}//if(m_wndReport.GetSafeHwnd() != NULL)
		}//if(xml->Load(m_sLangFN))
		delete xml;
	}//if(fileExists(m_sLangFN))	
}



void CSampleTreeView::setupDclsTable(int dcls)
{
	if(m_bPopulated == FALSE)
	{
		m_wndReport.BeginUpdate();

		m_wndReport.AddRecord(new CSampleTreeViewReportRec(csDefaultType));
		
		m_wndReport.EndUpdate();

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();

		m_bPopulated = TRUE;
	}	
}


BOOL CSampleTreeView::isFocusedRowEmpty()
{
	BOOL bResult = TRUE;
	CString csOut;
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	CXTPReportColumns *pCols = m_wndReport.GetColumns();
	CXTPReportRecords *pRec = m_wndReport.GetRecords();
	CSampleTreeViewReportRec *pActRec = (CSampleTreeViewReportRec*)pRec->GetAt(pRow->GetIndex());

	for(int i=0; i<pCols->GetCount(); i++)
	{
		if(i != ST_COL_TYPE)
			if(pActRec->getColumnFloat(i) != 0)
				bResult = FALSE;

	}

	return bResult;
}


void CSampleTreeView::OnValueChanged(NMHDR *pNotifyStruct, LRESULT *)
{
	CString csGCrownMsg;

	if(!m_wndReport.GetFocusedRow())
		return;

	XTP_NM_REPORTRECORDITEM *pNotifyItem = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	//check that green grown value <= 100 (%)
	if(pNotifyItem->pColumn->GetItemIndex() == ST_COL_GCROWN)
	{
		CSampleTreeViewReportRec *pRec = (CSampleTreeViewReportRec*)pNotifyItem->pItem->GetRecord();
		if(pRec->getColumnFloat(ST_COL_GCROWN) > MAX_GCROWN)
		{
			if(fileExists(m_sLangFN))
			{
				RLFReader*xml = new RLFReader;
				if(xml->Load(m_sLangFN))
				{
					csGCrownMsg = xml->str(IDS_STRING149);
				}
				delete xml;
			}

			AfxMessageBox(csGCrownMsg);
			pRec->setColumnFloat(ST_COL_GCROWN, 0);
			m_wndReport.Populate();
			m_wndReport.UpdateWindow();
			//data changed
			setDataChanged();
			return;
		}
	}
	
	//if on last row and row not empty, insert new empty row 
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if(pRows != NULL)
	{
		CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
		if(pRow != NULL)
		{
			if(pRow->GetIndex() == pRows->GetCount() -1)
			{
				if(isFocusedRowEmpty() == FALSE)
				{
					CSampleTreeViewReportRec *pRec = (CSampleTreeViewReportRec*)pRow->GetRecord();
					m_wndReport.AddRecord(new CSampleTreeViewReportRec(pRec->getColumnText(ST_COL_TYPE)));//csDefaultType));
					m_wndReport.Populate();
					m_wndReport.UpdateWindow();
					//data changed
					setDataChanged();
				}//if(FocusedRowEmpty)
			}//if(pRow->GetIndex() == pRows->GetCount() -1)
		}//if(pRow != NULL)
	}//if(pRows != NULL)

	setNumOfTreeHeader();
}

void CSampleTreeView::deleteSampleTree()
{
	CString csDelMsg;
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
			csDelMsg = xml->str(IDS_STRING150);
		delete xml;
	}

	CXTPReportSelectedRows* pRows = m_wndReport.GetSelectedRows();
	int count;
	int index, num_of_deleted = 0;
	BOOL bShowMsg = TRUE;


	if(pRows != NULL)
	{
		POSITION pos = pRows->GetFirstSelectedRowPosition();

		while(pos)
		{
			CXTPReportRow* pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				count = m_wndReport.GetRecords()->GetCount()-1;
				index = pRow->GetIndex() - num_of_deleted;
				
				if(index != count)	//don't delete last row
				{
					if(bShowMsg && AfxMessageBox(csDelMsg, MB_ICONQUESTION | MB_YESNO) != IDYES)
						break;

					bShowMsg = FALSE;

					m_wndReport.GetRecords()->RemoveAt(index);
					setDataChanged();
					num_of_deleted++;
					count--;
				}
			}
		}
	}

	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

	setNumOfTreeHeader();	
}

BOOL CSampleTreeView::getSampleTree(vecMySampleTrees &vecST, int id)
{
	BOOL bRes = TRUE;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CXTPReportColumns *pCols = m_wndReport.GetColumns();
	int type;
	BOOL bUse;

	for(int i=0; i<pRecs->GetCount(); i++)
	{
		CSampleTreeViewReportRec *pRec = (CSampleTreeViewReportRec*)pRecs->GetAt(i);
		if(!pRec)
			return FALSE;

		//check that not all columns empty
		bUse = FALSE;
		for(int j=0; j<pCols->GetCount(); j++)
		{
			if(j != ST_COL_TYPE)
				if(pRec->getColumnFloat(j) != 0)
				{
					bUse = TRUE;
					break;
				}//if(i != ST_COL_TYPE)
		}//for(j)

		if(bUse)
		{
			if((type = findTypeIndexAsInt(pRec->getColumnText(ST_COL_TYPE))) < 0)
				return FALSE;

			if(type == 0 || type == 1 || type == 2)
			{
				//minimum diameter and height
				if(pRec->getColumnFloat(ST_COL_DBH) == 0 || pRec->getColumnFloat(ST_COL_HGT) == 0)
				{
					AfxMessageBox(m_csErrMsgSampleTree, MB_ICONERROR);
					return FALSE;
				}
			}
			else if(type == 3)
			{
				//minimum diameter, age and SI
				if(pRec->getColumnFloat(ST_COL_DBH) == 0 || pRec->getColumnFloat(ST_COL_AGE) == 0 || pRec->getColumnFloat(ST_COL_BONITET) == 0)
				{
					AfxMessageBox(m_csErrMsgRandTree, MB_ICONERROR);
					return FALSE;
				}
			}

			vecST.push_back(CMyTransactionSampleTree(id,pRec->getColumnFloat(ST_COL_DBH),pRec->getColumnFloat(ST_COL_HGT),pRec->getColumnFloat(ST_COL_GCROWN),pRec->getColumnFloat(ST_COL_BARK),pRec->getColumnFloat(ST_COL_AGE),pRec->getColumnFloat(ST_COL_BONITET),type));
			bRes = TRUE;
		}

	}//for(i)

	return bRes;
}


int CSampleTreeView::findTypeIndexAsInt(LPCTSTR name)
{
	for(int i=0; i<m_sarrTreeTypes.GetCount(); i++)
	{
		if(m_sarrTreeTypes.GetAt(i).Compare(name) == 0)
			return i;
	}

	return -1;
}

void CSampleTreeView::updateRecord()
{
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

int CSampleTreeView::getNumOfTrees()
{
	int nTot = 0;

	CXTPReportColumns *pCols = m_wndReport.GetColumns();
	CXTPReportRecords *pRecs = NULL;
	CSampleTreeViewReportRec *pActRec = NULL;
	BOOL bResult;

	pRecs = m_wndReport.GetRecords();
	if(pRecs)
	{
		for(int j=0; j<pRecs->GetCount()-1; j++)
		{
			pActRec = (CSampleTreeViewReportRec*)pRecs->GetAt(j);

			bResult = FALSE;
			for(int i=0; i<pCols->GetCount(); i++)
			{
				if(i != ST_COL_TYPE)
					if(pActRec->getColumnFloat(i) != 0.0)
					{
						bResult = TRUE;
						break;
					}
			}

			if(bResult)
				nTot++;
		}
	}

	return nTot;
}

void CSampleTreeView::OnReportItemRClick(NMHDR *pNotifyStruct, LRESULT *)
{
	if(isFocusedRowEmpty())
		return;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	ASSERT(pItemNotify->pColumn);

	CPoint ptClick = pItemNotify->pt;
	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	//create menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_COPY_ONE_ROW, m_csMenuCopy1);
	menu.AppendMenu(MF_STRING, ID_COPY_ROW_MULTIPLE, m_csMenuCopy2);
	
	
	//track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	switch(nMenuResult)
	{
	case ID_COPY_ONE_ROW:
		copyRowOnce();
		break;
	case ID_COPY_ROW_MULTIPLE:
		copyRowMultiple();
		break;
	}
}

void CSampleTreeView::copyRowOnce()
{
	CXTPReportSelectedRows* pRows = m_wndReport.GetSelectedRows();
	CXTPReportRow* pRow = NULL;
	CSampleTreeViewReportRec *pRec = NULL;
	BOOL bUpdate = FALSE;
	int index=0;
	POSITION pos;

	if(pRows != NULL)
	{
		pos = pRows->GetFirstSelectedRowPosition();

		while(pos)
		{
			pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				index = pRow->GetIndex();
			}
		}

		index++;	//start position of insertion

		pos = pRows->GetFirstSelectedRowPosition();

		while(pos)
		{
			pRow = pRows->GetNextSelectedRow(pos);
			if(pRow)
			{
				pRec = (CSampleTreeViewReportRec*)pRow->GetRecord();
				if(pRec)
				{
					m_wndReport.GetRecords()->InsertAt(index++, new CSampleTreeViewReportRec(pRec));
					bUpdate = TRUE;
				}
			}
		}
	}

	if(bUpdate)
	{
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
		setNumOfTreeHeader();
	}
}

void CSampleTreeView::copyRowMultiple()
{
	CXTPReportSelectedRows* pRows = m_wndReport.GetSelectedRows();
	CXTPReportRow* pRow = NULL;
	CSampleTreeViewReportRec *pRec = NULL;
	BOOL bUpdate = FALSE;
	int index=0;
	POSITION pos;

	CNumOfCopiesDlg *dlg = new CNumOfCopiesDlg();

	if(dlg->DoModal() == IDOK)
	{
		int num = dlg->m_nNum;
		
		if(pRows != NULL)
		{
			pos = pRows->GetFirstSelectedRowPosition();

			while(pos)
			{
				pRow = pRows->GetNextSelectedRow(pos);
				if(pRow)
				{
					index = pRow->GetIndex();
				}
			}

			index++;	//start position of insertion
			for(int i=0; i<num; i++)
			{
				pos = pRows->GetFirstSelectedRowPosition();

				while(pos)
				{
					pRow = pRows->GetNextSelectedRow(pos);
					if(pRow)
					{
						pRec = (CSampleTreeViewReportRec*)pRow->GetRecord();
						if(pRec)
						{
							m_wndReport.GetRecords()->InsertAt(index++, new CSampleTreeViewReportRec(pRec));
							bUpdate = TRUE;
						}
					}
				}
			}
		}
	}

	if(bUpdate)
	{
		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
		setNumOfTreeHeader();
	}

	delete dlg;
}

void CSampleTreeView::setNumOfTreeHeader()
{
	//count number of trees and show in tab
	int nNum = getNumOfTrees();
	CString csTitle = _T(""), csOldTitle = _T("");
	int nCurrent;

	CStandTableView *pView = (CStandTableView*)getFormViewByID(IDD_FORMVIEW);
	if(pView)
	{
		CMyTabControl *pTab = &pView->m_wndTabControl;
		if(pTab)
		{
			CXTPTabManagerItem *pItem = pTab->getTabPage(pTab->GetCurSel());

			if(pItem)
			{
					CSampleTreeTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));
					
					if(pTabManager)
					{
						
						nCurrent = pTabManager->m_wndTabControl.GetCurSel();
						CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(nCurrent);
						if(pManager)
						{
							csOldTitle = pManager->GetTooltip();
							if(nNum > 0)
								csTitle.Format(_T("%s (%d)"), csOldTitle, nNum);
							else
								csTitle.Format(_T("%s"), csOldTitle);
							pManager->SetCaption(csTitle);
						}
					}
			}
		}//if(pTab)
	}
	
	//enable\disable export button
	CMDIStandTableFrame *pFrame = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pFrame)
	{
		pFrame->enableExportBtn();
	}
}

void CSampleTreeView::addTree(CMyTransactionSampleTree *tree)
{
	int index = 0;

	//insert before empty row (last row)
	index = m_wndReport.GetRecords()->GetCount();
	index -= 1;

	CString name;
	name = m_sarrTreeTypes.GetAt(tree->getType());
	tree->setTypeName(name);

	m_wndReport.GetRecords()->InsertAt(index, new CSampleTreeViewReportRec(tree));
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	
	//data changed
	setDataChanged();
}
