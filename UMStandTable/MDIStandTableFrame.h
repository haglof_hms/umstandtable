#pragma once

#include "GeneralDataDlg.h"
#include "SettingsDataDlg.h"

// CMDIFrameDoc document

class CMDIFrameDoc : public CDocument
{
	DECLARE_DYNCREATE(CMDIFrameDoc)

public:
	CMDIFrameDoc();
	virtual ~CMDIFrameDoc();
#ifndef _WIN32_WCE
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual BOOL OnNewDocument();

	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

///////////////////////////////////////////////////////////////////////////////////////////


#pragma once


// CMDIStandTableFrame frame

class CMDIStandTableFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CMDIStandTableFrame)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	HINSTANCE m_hInst;

	CXTPDockingPaneManager m_paneManager;
	CXTPToolBar m_wndToolBar;
	RECT toolbarRect;

	void ToggleGeneralDataDlg(void);
	void ToggleSettingsDataDlg(void);

	void setLanguage(void);

	CGeneralDataDlg m_wndGeneralDataDlg;
	CSettingsDataDlg m_wndSettingsDataDlg;

protected:
	CMDIStandTableFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIStandTableFrame();

	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;

	BOOL isOkToClose(void);
	BOOL speciesIsSelected(void);	//new v1.0.12
	

public:
	CXTPDockingPaneManager* GetDockingPaneManager()
	{
		return &m_paneManager;
	}

	static XTPDockingPanePaintTheme m_themeCurrent;

	void enableExportBtn();

protected:
	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnPaint();
	afx_msg void OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnDockingPaneNotify(WPARAM wParam, LPARAM lParam);

	//Toolbar buttons event
	afx_msg void OnTBtnOpenGeneralData(void);
	afx_msg void OnTBtnOpenSettingsData(void);
	afx_msg void OnTBtnCreateFile(void);
	afx_msg void OnTBtnImportSampleTrees(void);
	afx_msg void OnTBtnAddSpecie(void);
	afx_msg void OnTBtnRemoveSpecie(void);
	afx_msg void OnTBtnPreview(void);
	afx_msg void OnClose();
};


