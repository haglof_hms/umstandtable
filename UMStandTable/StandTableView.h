#pragma once
#include "TabPageManager.h"
#include "SampleTreeTabPageManager.h"

class CMDIFrameDoc;
// CStandTableView view

class CStandTableView : public CView
{
	DECLARE_DYNCREATE(CStandTableView)

	CString m_sAbrevLangSet;
	CString m_sLangFN;
	CString m_csFileMsg;
	CString m_csErrCreateInvFile;
	CString m_csErrMsgNoTree;

	CMyTabControl m_wndTabControl;		//changed from CXTPTabControl m_wndTabControl;

	vecTransactionSpecies m_vecSpecies;
	vecInt m_vecSpeciesUsed;
	vecMySampleTrees m_vecSampleTrees;

	DB_CONNECTION_DATA m_dbConnectionData;
	BOOL m_bConnected;
	
protected:
	CStandTableView();           // protected constructor used by dynamic creation
	virtual ~CStandTableView();

	CString getTreeDataHxl();
	BOOL isSampleTreeSet(double id, double start, double stop, int type, CMyTransactionSampleTree &stree);
	BOOL getSampleTrees(void);
	int getNumOfSpcUsed(void);
	CString getSpcNamesUsedHxl(void);
	BOOL CreateFile(CString &str, LPCTSTR fileext);
	BOOL isLibraryCreated(CString &str);

public:
	void updateRecords(void);
	int getNumOfSpc(void);
	int getNumOfTrees(void);
	BOOL addSpecieToStandTable(void);
	BOOL setupNewStandTable(BOOL bShowMsg);
	BOOL saveDataToHxlFile(void);
	int removeSpecieFromStandTable(void);

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
	CMDIFrameDoc* GetDocument() { return (CMDIFrameDoc*)m_pDocument;}
	void setupDclsTables(double dcls);
	void deleteFromStandTable(void);
	void addDclsClasses(void);
	BOOL isOkToClose(void);
	void resetDataChanged(void);
	BOOL openInvHxlFile(void);
public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual void OnInitialUpdate();
};


