#pragma once
#include "Resource.h"
#include "MDIStandTableFrame.h"

// CTabPageManager form view

class CTabPageManager : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTabPageManager)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	CFont m_fntTab;

public:
	CMyTabControl m_wndTabControl;		//changed from CXTPTabControl m_wndTabControl;
	//vecTransactionSpecies m_vecSpecies;

//protected:
public:
	CTabPageManager();           // protected constructor used by dynamic creation
	virtual ~CTabPageManager();

public:
	enum { IDD = IDD_FORMVIEW1 };
	
	CMDIFrameDoc* GetDocument();
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int spc_id, int nIcon);
	void setupDclsTables(double dcls);
	void deleteFromStandTable(void);

	BOOL isOkToClose(void);
	void resetDataChanged(void);
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual void OnInitialUpdate();
	afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
	virtual void OnDraw(CDC* /*pDC*/);
};

