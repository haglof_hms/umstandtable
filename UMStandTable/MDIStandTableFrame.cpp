// MDIStandTableFrame.cpp : implementation file
//

#include "stdafx.h"
#include "MDIStandTableFrame.h"
#include "Resource.h"
#include "StandTableView.h"


// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

CMDIFrameDoc::CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

CMDIFrameDoc::~CMDIFrameDoc()
{
	//AfxMessageBox(_T("~CMDIFrameDoc()"));
}


BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
END_MESSAGE_MAP()


// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

#ifndef _WIN32_WCE
void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif
#endif //_DEBUG

#ifndef _WIN32_WCE
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}
#endif

// CMDIFrameDoc commands


/////////////////////////////////////////////////////////////////////////////
// CMDIStandTableFrame

XTPDockingPanePaintTheme CMDIStandTableFrame::m_themeCurrent = xtpPaneThemeOffice2003;

IMPLEMENT_DYNCREATE(CMDIStandTableFrame, CChildFrameBase)

CMDIStandTableFrame::CMDIStandTableFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIStandTableFrame::~CMDIStandTableFrame()
{
	TRACE("~CMDIStandTableFrame()");
}


BEGIN_MESSAGE_MAP(CMDIStandTableFrame, CChildFrameBase)
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_WM_PAINT()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_GETMINMAXINFO()
	ON_WM_SIZE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_DESTROY()
	ON_MESSAGE(XTPWM_DOCKINGPANE_NOTIFY, OnDockingPaneNotify)
	
	//Toolbar buttons event
	ON_COMMAND(ID_TBTN_GENERAL, OnTBtnOpenGeneralData)
	ON_COMMAND(ID_TBTN_SETTINGS, OnTBtnOpenSettingsData)
	ON_COMMAND(ID_TBTN_EXPFILE, OnTBtnCreateFile)
	ON_COMMAND(ID_TBTN_IMPTREE, OnTBtnImportSampleTrees)
	ON_COMMAND(ID_TBTN_INCSPC, OnTBtnAddSpecie)
	ON_COMMAND(ID_TBTN_DECSPC, OnTBtnRemoveSpecie)
	ON_COMMAND(ID_TBTN_PREVIEW, OnTBtnPreview)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMDIStandTableFrame message handlers

BOOL CMDIStandTableFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CChildFrameBase::PreCreateWindow(cs))
		return FALSE;
	
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	return TRUE;
}

int CMDIStandTableFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString csToolTip1;
	CString csToolTip2;
	CString csToolTip3;
	CString csToolTip4;
	CString csToolTip5;
	CString csToolTip6;
	CString csToolTip7;

	if (CXTPFrameWndBase<CMDIChildWnd>::OnCreate(lpCreateStruct) == -1)
		return -1;

	if(m_hIcon)
	{
		SetIcon(m_hIcon, TRUE);
		SetIcon(m_hIcon, FALSE);
	}

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csToolTip1 = xml->str(IDS_STRING120);
			csToolTip2 = xml->str(IDS_STRING121);
			csToolTip3 = xml->str(IDS_STRING122);
			csToolTip4 = xml->str(IDS_STRING123);
			csToolTip5 = xml->str(IDS_STRING124);
			csToolTip6 = xml->str(IDS_STRING125);
			csToolTip7 = xml->str(IDS_STRING188);
		}
		delete xml;
	}
	
	CXTPPaintManager::SetTheme(xtpThemeOffice2003);

	//create toolbar
	m_wndToolBar.CreateToolBar(WS_TABSTOP | WS_VISIBLE | WS_CHILD, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);


	//set tooltips, and icons 
	CXTPControl *pCtrl = NULL;
	HICON hIcon = NULL;
	CString csResFN = getToolBarResFN();

	if(fileExists(csResFN))
	{
		CXTPToolBar *pToolBar = &m_wndToolBar;
		if(pToolBar->IsBuiltIn())
		{
			if(pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				if(nBarID == IDR_TOOLBAR1)
				{
					pCtrl = p->GetAt(0);
					pCtrl->SetTooltip(csToolTip1);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_INFO);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					pCtrl->SetTooltip(csToolTip2);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_GENERAL);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(2);
					pCtrl->SetTooltip(csToolTip3);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_EXPORT);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);
					pCtrl->SetEnabled(FALSE);

					pCtrl = p->GetAt(3);
					pCtrl->SetTooltip(csToolTip4);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_IMPORT);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);
					pCtrl->SetVisible(FALSE);	//TODO: don't show for now

					pCtrl = p->GetAt(4);
					pCtrl->SetTooltip(csToolTip5);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_ADD);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(5);
					pCtrl->SetTooltip(csToolTip6);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_DEL);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(6);
					pCtrl->SetTooltip(csToolTip7);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_PREVIEW);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);

				}//if(nBarID == IDR_TOOLBAR1)
			}//if(pToolBar->GetType() != xtpBarTypeMenuBar)
		}//if(pToolBar->IsBuiltIn())
	}//if(fileExists(csResFN))

	//initialize pane manager
	m_paneManager.InstallDockingPanes(this);

	EnableDocking(CBRS_ALIGN_ANY);

	//create panes
	CXTPDockingPane *paneSettingsData = m_paneManager.CreatePane(IDC_SETTINGS_PANE, CRect(0,0,140,40), xtpPaneDockLeft);
	paneSettingsData->SetOptions(xtpPaneNoCloseable);
	
	CXTPDockingPane *paneGeneralData = m_paneManager.CreatePane(IDC_GENERAL_DATA_PANE,CRect(0,0,0,140), xtpPaneDockTop);
	paneGeneralData->SetOptions(xtpPaneNoCloseable);

	m_paneManager.SetTheme(m_themeCurrent);


	//NB! The GeneralDataDlg and SettingsDataDlg are only created if the corresponding pane is visible
	//CMDIStandTableFrame::OnDockingPaneNotify creates dialogs
	
	//load saved state for docking panes
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	if(layoutNormal.Load(REG_STANDTABLE_LAYOUT_KEY))
	{
		m_paneManager.SetLayout(&layoutNormal);
	}


	//set dialog title
	setLanguage();

	m_bFirstOpen = TRUE;

	return 0;
}

void CMDIStandTableFrame::enableExportBtn()
{
	int nRes = 0;
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos != NULL)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView != NULL)
			{
				nRes = pView->getNumOfTrees();
			}
		}
	}

	CString csName;
	csName = m_wndGeneralDataDlg.getName();

	BOOL bEnable = FALSE;
	if(csName != _T("") && nRes > 0)
	{
		bEnable = TRUE;
	}

	CXTPControl *pCtrl = NULL;
	CXTPToolBar *pToolBar = &m_wndToolBar;

	if(pToolBar)
	{
		UINT nBarID = pToolBar->GetBarID();

		if(nBarID == IDR_TOOLBAR1)
		{
			CXTPControls *p = pToolBar->GetControls();

			pCtrl = p->GetAt(2);
			pCtrl->SetEnabled(bEnable);
		}
	}
}


void CMDIStandTableFrame::OnSetFocus(CWnd* pOldWnd)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	if(speciesIsSelected() == TRUE)	//changed v1.0.12
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	else
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CXTPFrameWndBase<CMDIChildWnd>::OnSetFocus(pOldWnd);
}

void CMDIStandTableFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if(m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK | LM_HORZ | LM_COMMIT);
		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}

	CChildFrameBase::OnPaint();
}



void CMDIStandTableFrame::OnMDIActivate(BOOL bActivate, CWnd* pActivateWnd, CWnd* pDeactivateWnd)
{
	CChildFrameBase::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}


void CMDIStandTableFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	//load placement
	if(bShow && !IsWindowVisible() && m_bFirstOpen)
	{
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"),REG_ROOT, REG_CREATE_STANDTABLE_WP);
		LoadPlacement(this,csBuf);
	}
}

void CMDIStandTableFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_STANDTABLE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_STANDTABLE;

	CXTPFrameWndBase<CMDIChildWnd>::OnGetMinMaxInfo(lpMMI);
}

void CMDIStandTableFrame::OnSize(UINT nType, int cx, int cy)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

LRESULT CMDIStandTableFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	CDocument *pDoc = GetActiveDocument();

	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while(pos)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE, wParam, lParam);
		} 
	}

	return 0L;
}


void CMDIStandTableFrame::OnDestroy()
{
	//save window placement
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REG_CREATE_STANDTABLE_WP);
	SavePlacement(this, csBuf);

	//save layout on docking pane
	CXTPDockingPaneLayout layoutNormal(&m_paneManager);
	m_paneManager.GetLayout(&layoutNormal);
	layoutNormal.Save(REG_STANDTABLE_LAYOUT_KEY);

}

void CMDIStandTableFrame::OnClose()
{
	//check that changed data is saved to file
	//ask if to close if not saved	
	if(!isOkToClose())
	{
		CString csClose;
		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				csClose.Format(_T("%s\r\n\r\n%s\r\n\r\n%s"),
					xml->str(IDS_STRING156), xml->str(IDS_STRING157), xml->str(IDS_STRING158));
			}
			delete xml;
		}

		if(AfxMessageBox(csClose, MB_ICONEXCLAMATION | MB_YESNO) != IDYES)
		{
			//don't close window
			return;
		}
	}

	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	
	CXTPFrameWndBase<CMDIChildWnd>::OnClose();
}


LRESULT CMDIStandTableFrame::OnDockingPaneNotify(WPARAM wParam, LPARAM lParam)
{
	if(wParam == XTP_DPN_SHOWWINDOW)
	{
		CXTPDockingPane *pPane = (CXTPDockingPane*)lParam;
		if(!pPane->IsValid())
		{
			if(pPane->GetID() == IDC_GENERAL_DATA_PANE)
			{
				if(!m_wndGeneralDataDlg.GetSafeHwnd())
				{
					//create and attach dialog
					if(m_wndGeneralDataDlg.Create(CGeneralDataDlg::IDD, this))
					{
						pPane->Attach(&m_wndGeneralDataDlg);
					}
				}
			}//if(pPane->GetID() == IDC_GENERAL_DATA_PANE)
			else if(pPane->GetID() == IDC_SETTINGS_PANE)
			{
				if(!m_wndSettingsDataDlg.GetSafeHwnd())
				{
					//create and attach dialog
					if(m_wndSettingsDataDlg.Create(CSettingsDataDlg::IDD,this))
					{
						pPane->Attach(&m_wndSettingsDataDlg);

						//check if there is species selected allready, if so disable dcls value
						int nRes = 0;
						CDocument *pDoc = GetActiveDocument();
						if(pDoc != NULL)
						{
							POSITION pos = pDoc->GetFirstViewPosition();
							if(pos != NULL)
							{
								CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
								if(pView != NULL)
								{
									nRes = pView->getNumOfSpc();
								}
							}
						}
						
						if(nRes > 0)
							m_wndSettingsDataDlg.setEditDisabled(TRUE);
					}
				}
			}//else if(pPane->GetID() == IDC_SETTINGS_PANE)
		}//if(!pPane->IsValid())
		return TRUE;
	}

	return FALSE;
}

void CMDIStandTableFrame::setLanguage(void)
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			// Set caption on General Data Pane
			CXTPDockingPane* pPaneGeneralData = GetDockingPaneManager()->FindPane(IDC_GENERAL_DATA_PANE);
			if(pPaneGeneralData)
			{
				pPaneGeneralData->SetTitle(xml->str(IDS_STRING100));
				//pPaneGeneralData->Hide();
			}

			// Set caption on Settings Pane
			CXTPDockingPane *pPaneSettingsData = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
			if(pPaneSettingsData)
			{
				pPaneSettingsData->SetTitle(xml->str(IDS_STRING110));
				//pPaneSettingsData->Hide();
			}
		}
		delete xml;
	}
}


void CMDIStandTableFrame::ToggleGeneralDataDlg()
{
	CXTPDockingPane *pPane = GetDockingPaneManager()->FindPane(IDC_GENERAL_DATA_PANE);
	if(pPane)
	{
		m_paneManager.ToggleAutoHide(pPane);
	}
}

void CMDIStandTableFrame::ToggleSettingsDataDlg()
{
	CXTPDockingPane *pPane = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
	if(pPane)
	{
		m_paneManager.ToggleAutoHide(pPane);
	}
}

void CMDIStandTableFrame::OnTBtnOpenGeneralData()
{
	ToggleGeneralDataDlg();
}

void CMDIStandTableFrame::OnTBtnOpenSettingsData()
{
	ToggleSettingsDataDlg();
}

void CMDIStandTableFrame::OnTBtnCreateFile()
{
	CString csErrMsg;
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			csErrMsg = xml->str(IDS_STRING138);
		}
		delete xml;
	}

	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos != NULL)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView != NULL)
			{
				if(pView->saveDataToHxlFile() == FALSE)
				{
					//AfxMessageBox(csErrMsg);	//don't show msg
				}
			}
		}
	}
}


//Use this function to import an old inv or hxl file
void CMDIStandTableFrame::OnTBtnImportSampleTrees()
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos != NULL)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView != NULL)
			{
				if(pView->openInvHxlFile() == FALSE)
					;//TODO: error msg
			}
		}
	}
}

void CMDIStandTableFrame::OnTBtnAddSpecie()
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos != NULL)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView != NULL)
			{
				if(pView->addSpecieToStandTable() == TRUE)
					AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);	//new v1.0.12 enable delete button in shell toolbar
			}
		}
	}
}

void CMDIStandTableFrame::OnTBtnRemoveSpecie()
{
	int nNum = 1;

	CDocument *pDoc = GetActiveDocument();
	if(pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos != NULL)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView != NULL)
			{
				nNum = pView->removeSpecieFromStandTable();
			}
		}
	}

	if(nNum == 0)	//no species selected, make dcls field editable
	{
		CXTPDockingPane *pPane = GetDockingPaneManager()->FindPane(IDC_SETTINGS_PANE);
		if(pPane)
		{
			CSettingsDataDlg *pDlg = (CSettingsDataDlg*)pPane->GetChild();
			if(pDlg != NULL)
			{
				pDlg->setEditDisabled(FALSE);
			}
		}

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

	//enable\disable export button
	enableExportBtn();
}

BOOL CMDIStandTableFrame::isOkToClose()
{
	CDocument *pDoc = GetActiveDocument();
	if(pDoc)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView)
			{
				if(pView->isOkToClose() == FALSE)
					return FALSE;
			}//if(pView)
		}//if(pos)
	}//if(pDoc)
	
	return TRUE;
}

BOOL CMDIStandTableFrame::speciesIsSelected()
{
	BOOL bRes = FALSE;

	CDocument *pDoc = GetActiveDocument();
	if(pDoc)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		if(pos)
		{
			CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
			if(pView)
			{
				if(pView->getNumOfSpc() > 0)
					bRes = TRUE;
			}//if(pView)
		}//if(pos)
	}//if(pDoc)
	
	return bRes;

}

void CMDIStandTableFrame::OnTBtnPreview()
{
	showFormView(IDD_FORMVIEW3,m_sLangFN,0);
}