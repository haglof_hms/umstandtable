// GeneralDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GeneralDataDlg.h"
#include "SearchPropertyDlg.h"
#include "MDIStandTableFrame.h"


// CGeneralDataDlg dialog

IMPLEMENT_DYNAMIC(CGeneralDataDlg, CDialog)

CGeneralDataDlg::CGeneralDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGeneralDataDlg::IDD, pParent)
{
	m_bDataNotChanged = TRUE;
}

CGeneralDataDlg::~CGeneralDataDlg()
{
}

void CGeneralDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL10, m_wndLbl10);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);	
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);
	DDX_Control(pDX, IDC_EDIT9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT10, m_wndEdit10);

	DDX_Control(pDX, IDC_BTN_OPENPROP, m_wndBtnOpenPropWnd);
}


BEGIN_MESSAGE_MAP(CGeneralDataDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_EN_CHANGE(IDC_EDIT1, &CGeneralDataDlg::OnEnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT2, &CGeneralDataDlg::OnEnChangeEdit2)
	ON_EN_CHANGE(IDC_EDIT3, &CGeneralDataDlg::OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, &CGeneralDataDlg::OnEnChangeEdit4)
	ON_EN_CHANGE(IDC_EDIT5, &CGeneralDataDlg::OnEnChangeEdit5)
	ON_EN_CHANGE(IDC_EDIT6, &CGeneralDataDlg::OnEnChangeEdit6)
	ON_EN_CHANGE(IDC_EDIT7, &CGeneralDataDlg::OnEnChangeEdit7)
	ON_EN_CHANGE(IDC_EDIT8, &CGeneralDataDlg::OnEnChangeEdit8)
	ON_EN_CHANGE(IDC_EDIT9, &CGeneralDataDlg::OnEnChangeEdit9)
	ON_EN_CHANGE(IDC_EDIT10, &CGeneralDataDlg::OnEnChangeEdit10)
	ON_BN_CLICKED(IDC_BTN_OPENPROP, &CGeneralDataDlg::OnBnClickedBtnOpenprop)
	ON_EN_KILLFOCUS(IDC_EDIT10, &CGeneralDataDlg::OnEnKillfocusEdit10)
END_MESSAGE_MAP()


BEGIN_ANCHOR_MAP(CGeneralDataDlg)
    ANCHOR_MAP_ENTRY(IDC_LBL1,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL2,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL3,  ANF_TOP | ANF_LEFT)
    ANCHOR_MAP_ENTRY(IDC_LBL4,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL5,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL6,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL7,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL8,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL9,  ANF_TOP | ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_LBL10,  ANF_TOP | ANF_LEFT)
END_ANCHOR_MAP()


// CGeneralDataDlg message handlers

BOOL CGeneralDataDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CGeneralDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	setLanguage();
	
	m_wndEdit1.SetEnabledColor(BLACK, WHITE);
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit2.SetEnabledColor(BLACK, WHITE);
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit3.SetEnabledColor(BLACK, WHITE);
	m_wndEdit3.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit4.SetEnabledColor(BLACK, WHITE);
	m_wndEdit4.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit5.SetEnabledColor(BLACK, WHITE);
	m_wndEdit5.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit6.SetEnabledColor(BLACK, WHITE);
	m_wndEdit6.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit7.SetEnabledColor(BLACK, WHITE);
	m_wndEdit7.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit8.SetMask(_T(">##"));
	m_wndEdit8.SetPromptSymbol('_');
	m_wndEdit9.SetEnabledColor(BLACK, WHITE);
	m_wndEdit9.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit10.SetEnabledColor(BLACK, WHITE);
	m_wndEdit10.SetDisabledColor(BLACK, INFOBK);
	

	//m_wndEdit1.SetAsNumeric();
	//m_wndEdit2.SetAsNumeric();

	m_wndEdit4.SetAsNumeric();
	m_wndEdit5.SetAsNumeric();	//breddgrad
	m_wndEdit5.SetLimitText(3);
	m_wndEdit6.SetAsNumeric();	//h�jd �ver hav
	m_wndEdit6.SetLimitText(6);
	m_wndEdit7.SetAsNumeric();	//age
	m_wndEdit7.SetLimitText(4);
	m_wndEdit9.SetAsNumeric();
	m_wndEdit10.SetAsNumeric();	//Delnr

	m_wndBtnOpenPropWnd.SetBitmap(CSize(18,14), IDB_OPENPROP);
	m_wndBtnOpenPropWnd.SetXButtonStyle(BS_XT_WINXP_COMPAT);

	// --------------------------------------
	// At this point, we�ve set everything
	// up for our dialog except the 
	// anchoring/docking. We�ll do this now
	// with a call to InitAnchors()
	// --------------------------------------
	InitAnchors();

	resetDataChanged();

	return 0;
}

void CGeneralDataDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect rcWnd;
	GetWindowRect(&rcWnd);

	HandleAnchors(&rcWnd);
	Invalidate(false);
}

BOOL CGeneralDataDlg::OnEraseBkgnd(CDC* pDC)
{
	// Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

	return (m_bpfxAnchorMap.EraseBackground(pDC->m_hDC));
}

void CGeneralDataDlg::setLanguage(void)
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING101));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING102));
			m_wndLbl3.SetWindowText(xml->str(IDS_STRING103));
			m_wndLbl4.SetWindowText(xml->str(IDS_STRING104));
			m_wndLbl5.SetWindowText(xml->str(IDS_STRING105));
			m_wndLbl6.SetWindowText(xml->str(IDS_STRING106));
			m_wndLbl7.SetWindowText(xml->str(IDS_STRING107));
			m_wndLbl8.SetWindowText(xml->str(IDS_STRING108));
			m_wndLbl9.SetWindowText(xml->str(IDS_STRING109));
			m_wndLbl10.SetWindowText(xml->str(IDS_STRING191));

			m_csMsgSide = xml->str(IDS_STRING117);
			m_csMsgSILimit = xml->str(IDS_STRING118);
			m_csMsgSISpec = xml->str(IDS_STRING119);
			m_csMsgDelnr = xml->str(IDS_STRING194);
		}
		delete xml;
	}
}

void CGeneralDataDlg::OnEnChangeEdit1()
{
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit2()
{
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit3()
{
	//enable\disable export button
	CMDIStandTableFrame *pFrame = (CMDIStandTableFrame*)getFormViewParentByID(IDD_FORMVIEW);
	if(pFrame)
	{
		pFrame->enableExportBtn();
	}
	
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit4()
{
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit5()
{
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit6()
{
	//data changed, set flag
	setDataChanged();
}

void CGeneralDataDlg::OnEnChangeEdit7()
{
	//data changed, set flag
	setDataChanged();
}


void CGeneralDataDlg::OnEnChangeEdit8()
{
	CString csStr, csSpec, csValue;
	int value;

	if(m_wndEdit8.IsInputEmpty() == FALSE)
	{
		csStr.Format(_T("%s"), m_wndEdit8.GetInputData());
		csSpec = csStr.GetAt(0);
		

		if(csSpec != _T('_'))
		{
			if(csSpec == _T('T') || csSpec == _T('G') || csSpec == _T('B') || csSpec == _T('E') || csSpec == _T('F') || csSpec == _T('C'))
			{
				if(csStr.GetAt(1) != _T('_') && csStr.GetAt(2) != _T('_'))
				{
					csValue = csStr.Right(2);
					value = _tstoi(csValue);

					if((csSpec == _T('T') && (value < 10 || value > 32)) || (csSpec == _T('G') && (value < 10 || value > 40)) || ((csSpec != _T('T') && csSpec != _T('G')) && (value < 14 || value > 30)))
					{
						AfxMessageBox(m_csMsgSILimit);
						m_wndEdit8.EmptyData(TRUE);
						m_wndEdit8.SetFocus();
					}

					//data changed, set flag
					setDataChanged();
				}

			}
			else
			{
				AfxMessageBox(m_csMsgSISpec);
				m_wndEdit8.EmptyData(TRUE);
				m_wndEdit8.SetFocus();
			}
		}
	}	
}

void CGeneralDataDlg::OnEnChangeEdit9()
{
	CString csSide = _T("");
	csSide = m_wndEdit9.getText();
	if(csSide != _T(""))
	{
		if(csSide != _T("1") && csSide != _T("2"))
			{
			AfxMessageBox(m_csMsgSide);
			m_wndEdit9.SetWindowTextW(_T(""));
			m_wndEdit9.SetFocus();
		}
		else
		{
			//data changed, set flag
			setDataChanged();
		}
	}
}

void CGeneralDataDlg::OnEnChangeEdit10()
{
	//data changed, set flag
	setDataChanged();
}


void CGeneralDataDlg::OnBnClickedBtnOpenprop()
{
	CString csStr;
	CTransaction_property recSelProp;
	CSearchPropertyDlg *pDlg = new CSearchPropertyDlg();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			recSelProp = pDlg->getSelectedProperty();

			m_wndEdit3.SetWindowTextW(recSelProp.getPropertyName());
			m_wndEdit1.SetWindowTextW(recSelProp.getBlock());
			m_wndEdit2.SetWindowTextW(recSelProp.getUnit());
			csStr.Format(_T("%.3f"), recSelProp.getAreal());
			m_wndEdit4.SetWindowTextW(csStr);

			setDataChanged();
		}
		delete pDlg;
	}
}

void CGeneralDataDlg::setProperty(LPCTSTR sName)
{
	CString csProp = _T(""), csBlock = _T(""), csUnit = _T("");
	CString str(sName), partnum;
	//int pstart,delim;
	//set property, block and unit
	if(sName != _T(""))
	{
		csProp = sName;
		//TODO: property name
		/*
		// Extract part number from name (if any)
			// -Find block:unit delimiter
			str.Replace( '#', '-' );
			str.Replace( '_', '-' );
			delim = str.ReverseFind(':');
			if( delim != -1 )
			{
				// -Find part number delimiter
				pstart = str.Find('-', delim);
				if( pstart != -1 )
				{
					// -Extract part number, remove part number from name
					str = str.Mid(pstart+1).Trim();
					partnum = str.Left(pstart);
				}
				
				//TODO: Extract block and unit
				delim = str.ReverseFind(':');
				if(delim != -1)
				{
				csUnit = str.Right(str.GetLength() - delim+1);
				str = str.Mid(delim).Trim();
				}
				csProp = str;

			}
			else
			{
				csProp = str;
			}
		*/
		m_wndEdit3.SetWindowTextW(csProp);
		m_wndEdit1.SetWindowTextW(csBlock);
		m_wndEdit2.SetWindowTextW(csUnit);
		setDataChanged();
	}
}

void CGeneralDataDlg::setName(LPCTSTR sName)
{
	if(sName != _T(""))
	{
		m_wndEdit3.SetWindowTextW(sName);
		setDataChanged();
	}
}

void CGeneralDataDlg::setNumber(LPCTSTR sNum)
{
	if(sNum != _T(""))
	{
		m_wndEdit1.SetWindowTextW(sNum);
		setDataChanged();
	}
}

void CGeneralDataDlg::setSubNumber(LPCTSTR sNum)
{
	if(sNum != _T(""))
	{
		m_wndEdit2.SetWindowTextW(sNum);
		setDataChanged();
	}
}

void CGeneralDataDlg::setArea(int nArea)
{
	double area;
	if(nArea >= 0)
	{
		area = nArea/10000.0;	//m2 -> ha
		m_wndEdit4.setFloat(area,3);
		setDataChanged();
	}
}

void CGeneralDataDlg::setLatitude(int nLatitude)
{
	if(nLatitude >= 0)
	{
		m_wndEdit5.setInt(nLatitude);
		setDataChanged();
	}
}

void CGeneralDataDlg::setHeightOverSea(int nHgtSea)
{
	if(nHgtSea >= 0)
	{
		m_wndEdit6.setInt(nHgtSea);
		setDataChanged();
	}
}

void CGeneralDataDlg::setAge(int nAge)
{
	if(nAge >= 0)
	{
		m_wndEdit7.setInt(nAge);
		setDataChanged();
	}
}

void CGeneralDataDlg::setH100(LPCTSTR sSi)
{
	if(sSi != _T(""))
	{
		m_wndEdit8.SetWindowTextW(sSi);
		setDataChanged();
	}
}

void CGeneralDataDlg::setSide(int nSide)
{
	if(nSide == 1 || nSide == 2)
	{
		m_wndEdit9.setInt(nSide);
		setDataChanged();
	}
}

void CGeneralDataDlg::setDelnr(LPCTSTR sNum)
{
	if(sNum != _T(""))
	{
		m_wndEdit10.SetWindowTextW(sNum);
		setDataChanged();
	}
}

void CGeneralDataDlg::OnEnKillfocusEdit10()
{
	//UpdateData(TRUE);

	CString csDelnr;
	int nNum = 0;
	csDelnr = m_wndEdit10.getText();
	if(csDelnr != _T(""))
	{
		nNum = _tstoi(csDelnr);
		if(nNum == 0)
		{
			AfxMessageBox(m_csMsgDelnr);
			m_wndEdit10.SetWindowTextW(_T(""));
			m_wndEdit10.SetFocus();
		}
		else
		{
			//data changed, set flag
			setDataChanged();
		}
	}
}
