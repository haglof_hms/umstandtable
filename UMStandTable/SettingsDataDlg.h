#pragma once
#include "Resource.h"
#include "BPCtrlAnchorMap.h"

// CSettingsDataDlg dialog

class CSettingsDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CSettingsDataDlg)

	CString m_sAbrevLangSet;
	CString m_sLangFN;

	void setLanguage();

protected:
	CXTResizeGroupBox m_wndGrp1;
	CMyExtStatic m_wndLbl1;
	CMyExtEdit m_wndEdit1;

	CButton m_wndBtn1;	//button not used 091211
	//CButton m_wndBtn2;

public:
	CSettingsDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSettingsDataDlg();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR1 };

	//prevent enter and cancel on dlg
	virtual void OnOK() {}
	virtual void OnCancel() {}

	void clearDlg(void)
	{
		//m_wndEdit1.setFloat(/*2.0*/fDefDclsValue, 1);	//091211 spara diamterklassinställning
	}

	
	double getDcls(void)
	{
		return m_wndEdit1.getFloat();
	}

	double getAndCheckDcls(void);

	void setEditDisabled(BOOL bDisabled)
	{
		m_wndEdit1.SetReadOnly(bDisabled);
	}

	//CString getDcls(void) {return m_wndEdit1.getText();}

	// -------------------------------------
  // the following code declares the 
  // required elements for our AnchorMap
  // -------------------------------------
  DECLARE_ANCHOR_MAP();


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnChangeEdit1();
};
