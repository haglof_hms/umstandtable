// stdafx.cpp : source file that includes just the standard includes
// UMStandTable.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "UMStandTableDB.h"

void setupForDBConnection(HWND hWndRec, HWND hWndSend)
{
	if(hWndRec != NULL)
	{
		DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;
		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);

		data.conn = NULL;

		::SendMessage(hWndRec, WM_COPYDATA, (WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

CString getToolBarResFN()
{
	CString csPath;
	CString csToolBarFN;

	::GetModuleFileName(AfxGetApp()->m_hInstance, csPath.GetBufferSetLength(_MAX_PATH),_MAX_PATH);
	csPath.ReleaseBuffer();

	int nIndex = csPath.ReverseFind(_T('\\'));
	if(nIndex > 0)
		csPath = csPath.Left(nIndex + 1);
	else
		csPath.Empty();
	
	csToolBarFN.Format(_T("%s%s"),csPath, TOOLBAR_RES_DLL);

	return csToolBarFN;
}

BOOL getSpeciesFromDB(DB_CONNECTION_DATA &conn, vecTransactionSpecies &vec)
{
	CUMStandTableDB *pDB = new CUMStandTableDB(conn);
	if(pDB != NULL)
		pDB->getSpecieDataFromDB(vec);
	delete pDB;

	return (vec.size() > 0);
}

CString getSpcName(vecTransactionSpecies &vec, int spcid)
{
	if(vec.size() > 0)
	{
		for(UINT i=0; i<vec.size(); i++)
		{
			if(vec[i].getSpcID() == spcid)
				return vec[i].getSpcName();
		}
	}

	return NULL;
}

BOOL getPropertiesFromDB(DB_CONNECTION_DATA &conn, vecTransactionProperty &vec, LPCTSTR quest)
{
	CUMStandTableDB *pDB = new CUMStandTableDB(conn);
	if(pDB != NULL)
		pDB->getPropertyDataFromDB(vec, quest);
	delete pDB;

	return (vec.size() > 0);
}


CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
//					pView->SendMessage(WM_USER_MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
//						pView->SendMessage(WM_USER_MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}


CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}


CString getStanfordDllPath(void)
{
	CString sPath;
	CString sDllPath;

	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex = sPath.ReverseFind(_T('\\'));
	if(nIndex > 0)
		sPath = sPath.Left(nIndex + 1);
	else
		sPath.Empty();

	sDllPath.Format(_T("%s%s"),sPath,DLL_FILE_NAME);

	return sDllPath;
}


CString CheckStringNotForbidden(CString str)
{
	str.Replace(_T("&"),_T("&amp;"));
	return str;
}