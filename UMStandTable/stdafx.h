// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
 
#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif


#define WINVER 0x0500                 // Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <afxdllx.h>
#include <vector>

//#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h>

#include <SQLAPI.h>

#include <pad_hms_miscfunc.h>
#include <ResLangFileReader.h>
#include <pad_transaction_classes.h>
#include <xmllite.h>

typedef struct _db_connection_data
{
	SAConnection* conn;              // SAConnection �r SQLApi class
    SAConnection* admin_conn;
} DB_CONNECTION_DATA;


//functions in stdafx.cpp
void setupForDBConnection(HWND hWndRec, HWND hWndSend);
CString getToolBarResFN();
BOOL getSpeciesFromDB(DB_CONNECTION_DATA &conn, vecTransactionSpecies &vec);
CString getSpcName(vecTransactionSpecies &vec, int spcid);
BOOL getPropertiesFromDB(DB_CONNECTION_DATA &conn, vecTransactionProperty &vec, LPCTSTR quest);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp);
CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);
CString getStanfordDllPath(void);
CString CheckStringNotForbidden(CString str);

const LPCTSTR TBL_PROPERTY					= _T("fst_property_table");

//strings and definitions
typedef int (WINAPI *Func_FileOpen)(char *filename);
typedef int (WINAPI *Func_GetVersion)(char str[]);
typedef int (WINAPI *Func_GetNumOfSet)(int *variabel);
typedef int (WINAPI *Func_GetInteger)(int part, int var, int type, int index,int *variabel);
typedef int (WINAPI *Func_GetText)(int part, int var, int type, int index, char str[]);

const LPCTSTR FUNC_FILE_OPEN					= _T("StanfordFileOpen");
const LPCTSTR FUNC_GET_VERSION					= _T("StanfordGetVersion");
const LPCTSTR FUNC_GET_NUMOFSET					= _T("StanfordGetNumOfSet");
const LPCTSTR FUNC_GET_INTEGER					= _T("StanfordGetInteger");
const LPCTSTR FUNC_GET_TEXT						= _T("StanfordGetText");

const LPCTSTR DLL_FILE_NAME						= _T("Stanford.dll");				//functions for reading data from inv-file
const LPCTSTR PROGRAM_NAME						= _T("UMStandTable");				// Used for Languagefile, registry entries etc.; 051114 p�d
const LPCTSTR REG_CREATE_STANDTABLE_WP			= _T("UMStandTable\\CreateStandTable\\Placement");
const LPCTSTR REG_STANDTABLE_LAYOUT_KEY			= _T("UMStandTable_Layout");
const LPCTSTR REG_STANDTABLE_PREVIEW_LAYOUT_KEY	= _T("UMStandTable\\Preview\\PreView_Layout");
const LPCTSTR SUBDIR_HMS						= _T("HMS\\");
const LPCTSTR SUBDIR_DATA						= _T("Data\\");


const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		
#define RSTR_TB_INFO						20
#define RSTR_TB_GENERAL						40
#define RSTR_TB_IMPORT						19
#define RSTR_TB_EXPORT						9
#define RSTR_TB_ADD							0
#define RSTR_TB_DEL							28
#define RSTR_TB_PREVIEW						31
#define RSTR_TB_PRINT						36
const LPCTSTR sz0dec							= _T("%.0f");
const LPCTSTR sz1dec							= _T("%.1f");

//////////////////////////////////////////////////////////////////////////////////////////
// HTM file for Viewing Pricelist; 090901 p�d
const LPCTSTR	PRL_VIEW_FN							=	_T("FB2A0188-5D7F-408e-B4AB-176E35B07D63.htm");


//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s)

const int MIN_X_SIZE_STANDTABLE				= 700;
const int MIN_Y_SIZE_STANDTABLE				= 500;
const double MAX_DCLS						= 60.0;
const double MIN_DCLS						= 0.1;

const int MIN_X_SIZE_PREVIEW				= 450;
const int MIN_Y_SIZE_PREVIEW				= 300;

#define IDS_STRING100						100
#define IDS_STRING101						101
#define IDS_STRING102						102
#define IDS_STRING103						103
#define IDS_STRING104						104
#define IDS_STRING105						105
#define IDS_STRING106						106
#define IDS_STRING107						107
#define IDS_STRING108						108
#define IDS_STRING109						109
#define IDS_STRING110						110
#define IDS_STRING111						111
#define IDS_STRING112						112
#define IDS_STRING113						113
#define IDS_STRING114						114
#define IDS_STRING115						115
#define IDS_STRING116						116
#define IDS_STRING117						117
#define IDS_STRING118						118
#define IDS_STRING119						119
#define IDS_STRING120						120
#define IDS_STRING121						121
#define IDS_STRING122						122
#define IDS_STRING123						123
#define IDS_STRING124						124
#define IDS_STRING125						125
#define IDS_STRING126						126
#define IDS_STRING127						127
#define IDS_STRING128						128
#define IDS_STRING129						129
#define IDS_STRING130						130
#define IDS_STRING131						131	
#define IDS_STRING132						132
#define IDS_STRING133						133
#define IDS_STRING134						134
#define IDS_STRING135						135
#define IDS_STRING136						136
#define IDS_STRING137						137
#define IDS_STRING138						138
#define IDS_STRING139						139
#define IDS_STRING140						140
#define IDS_STRING141						141
#define IDS_STRING142						142
#define IDS_STRING143						143
#define IDS_STRING144						144
#define IDS_STRING145						145
#define IDS_STRING146						146
#define IDS_STRING147						147
#define IDS_STRING148						148
#define IDS_STRING149						149
#define IDS_STRING150						150
#define IDS_STRING151						151
#define IDS_STRING152						152
#define IDS_STRING153						153
#define IDS_STRING154						154
#define IDS_STRING155						155	
#define IDS_STRING156						156
#define IDS_STRING157						157
#define IDS_STRING158						158
#define IDS_STRING159						159
#define IDS_STRING160						160
#define IDS_STRING161						161
#define IDS_STRING162						162
#define IDS_STRING163						163
#define IDS_STRING164						164
#define IDS_STRING165						165
#define IDS_STRING166						166
#define IDS_STRING167						167
#define IDS_STRING168						168
#define IDS_STRING169						169
#define IDS_STRING170						170
#define IDS_STRING171						171
#define IDS_STRING172						172
#define IDS_STRING173						173
#define IDS_STRING174						174
#define IDS_STRING175						175
#define IDS_STRING176						176
#define IDS_STRING177						177
#define IDS_STRING178						178
#define IDS_STRING179						179
#define IDS_STRING180						180
#define IDS_STRING181						181
#define IDS_STRING182						182
#define IDS_STRING183						183
#define IDS_STRING184						184
#define IDS_STRING185						185
#define IDS_STRING186						186
#define IDS_STRING187						187
#define IDS_STRING188						188	
#define IDS_STRING189						189
#define IDS_STRING190						190
#define IDS_STRING191						191
#define IDS_STRING192						192
#define IDS_STRING193						193
#define IDS_STRING194						194

#define IDC_GENERAL_DATA_PANE				0x8101
#define IDC_SETTINGS_PANE					0x8102
#define IDC_TABCONTROL_1					0x8103
#define IDC_TABCONTROL_2					0x8104
#define IDC_REPORT							0x8105
#define IDC_SEL_PROP_REPORT					0x8106
#define ID_COPY_ONE_ROW						0x8107
#define ID_COPY_ROW_MULTIPLE				0x8108

const float fDefDclsValue = 2.0;

/////////////////////////////////////////////////////////////////////////////////////////////////
// enumrated column values used in:

enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22
};

enum{
	TAB_CUT = 0,
	TAB_STANDING = 1,
	TAB_CUT_TRACK = 2,
	TAB_SAMPLETREE = 3
};	//tab types

enum{
	DCLS_COL_START						= 0,
	DCLS_COL_END						= 1,
	DCLS_COL_NUM_OF						= 2
};// dcls columns

enum{
	ST_COL_DBH							= 0,
	ST_COL_HGT							= 1,
	ST_COL_GCROWN						= 2,
	ST_COL_BARK							= 3,
	ST_COL_AGE							= 4,
	ST_COL_BONITET						= 5,
	ST_COL_TYPE							= 6,
	MAX_GCROWN							= 100	
};// sample tree variables

enum 
{
	COL_PROP_0,
	COL_PROP_1,
	COL_PROP_2,
	COL_PROP_3
};

//class sample tree
class CMyTransactionSampleTree
{
	int m_nSpcId;
	double m_fDbh;	//mm
	double m_fHgt; //dm
	double m_fGCrown;	//dm
	double m_fBrk;	//mm
	double m_fAge;	//years
	double m_fBonitet;
	int m_nType;
	CString m_csTypeName;

public:
	CMyTransactionSampleTree(void)
	{
		m_nSpcId = 1;
		m_fDbh = 0.0;
		m_fHgt = 0.0;
		m_fGCrown = 0.0;
		m_fBrk = 0.0;
		m_fAge = 0.0;
		m_fBonitet = 0.0;
		m_nType = 0;
		m_csTypeName = _T("");
	}

	CMyTransactionSampleTree(int spc, double dbh, double hgt, double gcrown, double brk, double age, double bonitet, int type, LPCTSTR type_name = _T(""))
	{
		m_nSpcId = spc;
		m_fDbh = dbh;
		m_fHgt = hgt;
		m_fGCrown = gcrown;
		m_fBrk = brk;
		m_fAge = age;
		m_fBonitet = bonitet;
		m_nType = type;
		m_csTypeName = type_name;
	}

	CMyTransactionSampleTree(const CMyTransactionSampleTree &c) { *this = c;}

	int getSpcId(void) { return m_nSpcId;}
	double getDbh(void) { return m_fDbh;}
	double getHgt(void) { return m_fHgt;}
	double getGCrown(void) { return m_fGCrown;}
	double getBrk(void) { return m_fBrk;}
	double getAge(void) { return m_fAge;}
	double getBonitet(void) { return m_fBonitet;}
	int getType(void) { return m_nType;}
	CString getTypeName(void) {return m_csTypeName;}
	void setTypeName(CString name) { m_csTypeName = name;}
};

typedef std::vector<CMyTransactionSampleTree> vecMySampleTrees;


#define NUM_OF_MSPEC 50

struct SPECNAMES
{
	TCHAR name[20];
	int id;
	int use;
};