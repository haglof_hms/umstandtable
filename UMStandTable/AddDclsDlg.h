#pragma once

#include "Resource.h"

//class not used in this version
//instead add diameter class by clicking on the lasr row in the dcls table

// CAddDclsDlg dialog

class CAddDclsDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddDclsDlg)

	CString m_sAbrevLangSet;
	CString m_sLang;

protected:
	CXTResizeGroupBox m_wndGrp1;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;

	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

public:
	CAddDclsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddDclsDlg();

	void setDcls(int dcls) { m_wndEdit1.setInt(dcls);}
	int getDcls(void) { return m_wndEdit1.getInt();}

	void setNumOfClasses(int num) { m_wndEdit2.setInt(num);}
	int getNumOfClasses(void) { return m_wndEdit2.getInt();}


// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	void setLanguage(void);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual BOOL OnInitDialog();
};
