#pragma once

#include "Resource.h"
#include "MyRichEditCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CELVPrlViewAndPrintRec

//////////////////////////////////////////////////////////////////////////////////////////
// Type of fonts in CELVPrlViewAndPrintRec; 090107 p�d
#define FNT_SMALL						0
#define FNT_SMALL_UNDERLINE	1
#define FNT_SMALL_BOLD			2
#define FNT_NORMAL					3
#define FNT_NORMAL_BOLD			4
#define FNT_HEADER					5

class CELVPrlViewAndPrintRec : public CXTPReportRecord
{
	//private:
protected:

	class CExtTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		CFont m_fntSmall;
		CFont m_fntSmallUnderline;
		CFont m_fntSmallBold;
		CFont m_fnt;
		CFont m_fntBold;
		CFont m_fntHeader;
	public:
		CExtTextItem(CString sValue) : 
				CXTPReportRecordItemText(sValue)
		{
			m_fntSmall.CreateFont(14, 0, 0, 0, FW_NORMAL,
													 FALSE, FALSE, FALSE, 0, 
													 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
													 _T("Courier New"));
			m_fntSmallUnderline.CreateFont(14, 0, 0, 0, FW_NORMAL,
													 FALSE, TRUE, FALSE, 0, 
													 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
													 _T("Courier New"));
			m_fntSmallBold.CreateFont(14, 0, 0, 0, FW_BOLD,
																FALSE, FALSE, FALSE, 0, 
																OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, 
																_T("Courier New"));
			m_fnt.CreateFont(17, 0, 0, 0, FW_NORMAL,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_fntBold.CreateFont(17, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_fntHeader.CreateFont(20, 0, 0, 0, FW_BOLD,
										 FALSE, FALSE, FALSE, 0, 
										 OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_ROMAN, 
										 _T("Times New Roman"));
			m_sText = sValue;
		}
		virtual ~CExtTextItem(void)
		{
			m_fntSmall.DeleteObject();
			m_fntSmallUnderline.DeleteObject();
			m_fntSmallBold.DeleteObject();
			m_fnt.DeleteObject();
			m_fntBold.DeleteObject();
			m_fntHeader.DeleteObject();
		}

		// Set "normal" font
		void setFontSmall(COLORREF col,COLORREF text_col)	{ SetFont(&m_fntSmall);	SetBackgroundColor(col); SetTextColor( text_col); }
		void setFontSmallUnderline(COLORREF col,COLORREF text_col)	{ SetFont(&m_fntSmallUnderline); SetBackgroundColor(col);	SetTextColor( text_col);}
		void setFontSmallBold(COLORREF col,COLORREF text_col)	{ SetFont(&m_fntSmallBold); SetBackgroundColor(col); SetTextColor( text_col);	}
		void setFont(COLORREF col,COLORREF text_col)	{ SetFont(&m_fnt); SetBackgroundColor(col);	SetTextColor( text_col);}
		void setFontBold(COLORREF col,COLORREF text_col)	{ SetFont(&m_fntBold); SetBackgroundColor(col);	SetTextColor( text_col);}
		void setFontHeader(COLORREF col,COLORREF text_col)	{ SetFont(&m_fntHeader);	SetBackgroundColor(col); SetTextColor( text_col); }

	};
	
public:
	CELVPrlViewAndPrintRec(void)
	{
		AddItem(new CExtTextItem(_T("")));
	}

	CELVPrlViewAndPrintRec(LPCTSTR text)
	{
		AddItem(new CExtTextItem((text)));
	}

	void setColumnFont(int fnt_num,COLORREF col = WHITE,COLORREF text_col = BLACK)	
	{ 
		if (fnt_num == FNT_SMALL)	// Normal font
			((CExtTextItem*)GetItem(COLUMN_0))->setFontSmall(col,text_col);
		else if (fnt_num == FNT_SMALL_UNDERLINE)	// Normal font
			((CExtTextItem*)GetItem(COLUMN_0))->setFontSmallUnderline(col,text_col);
		else if (fnt_num == FNT_SMALL_BOLD)	// Normal font
			((CExtTextItem*)GetItem(COLUMN_0))->setFontSmallBold(col,text_col);
		else if (fnt_num == FNT_NORMAL)	// Normal font
			((CExtTextItem*)GetItem(COLUMN_0))->setFont(col,text_col);
		else if (fnt_num == FNT_NORMAL_BOLD)	// Bold font
			((CExtTextItem*)GetItem(COLUMN_0))->setFontBold(col,text_col);
		else if (fnt_num == FNT_HEADER)	// Header font
			((CExtTextItem*)GetItem(COLUMN_0))->setFontHeader(col,text_col);
	}

};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrinttDoc

class CMDIPrlViewAndPrintDoc : public CDocument
{
protected: // create from serialization only
	CMDIPrlViewAndPrintDoc();
	DECLARE_DYNCREATE(CMDIPrlViewAndPrintDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMDIPrlViewAndPrinttDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMDIPrlViewAndPrintDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMDIPrlViewAndPrinttDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintFrame frame

class CMDIPrlViewAndPrintFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CMDIPrlViewAndPrintFrame)

//private:
	CString m_sLangFN;

	BOOL m_bIsSettingsTBtn;

	CXTPToolBar m_wndToolBar;

	CXTPDockingPaneManager m_paneManager;

	RECT toolbarRect;

	CString m_sExchangeTemplate;
		
	void setLanguage(void);

protected:

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CMDIPrlViewAndPrintFrame();           // protected constructor used by dynamic creation
	virtual ~CMDIPrlViewAndPrintFrame();

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	void setSettingsTBtn(BOOL v)	{ m_bIsSettingsTBtn = v; }

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnSetFocus(CWnd *);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPrintOut(void);
	afx_msg void OnSettings(void);
	afx_msg void OnSettingsTBtn(CCmdUI*);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint
class CPrlViewAndPrint : public CMyHtmlView
{
protected:
	CPrlViewAndPrint();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPrlViewAndPrint)

	CString m_sLangFN;

	BOOL m_bUseSettings;
	BOOL m_bShowPriceByQual;
// html Data
public:
	//{{AFX_DATA(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

	void printOut(void);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrlViewAndPrint)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void setupHTM(void);
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPrlViewAndPrint();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CPrlViewAndPrint)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


