//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMStandTable.rc
//
#define IDD_FORMVIEW                    870
#define IDD_FORMVIEW3                   871
#define IDC_EDIT17                      11033
#define IDC_LBL_DLGBAR1                 11162
#define IDC_LBL_DLGBAR2                 11163
#define IDC_LBL_DLGBAR3                 11164
#define IDC_LBL_DLGBAR4                 11165
#define IDC_LBL_DLGBAR5                 11166
#define IDC_LBL_DLGBAR6                 11167
#define IDC_GRP_DLGBAR                  11168
#define IDD_FORMVIEW1                   11169
#define IDD_DIALOGBAR                   11170
#define IDD_FORMVIEW2                   11171
#define IDD_DIALOGBAR2                  11172
#define IDD_DIALOGBAR1                  11173
#define IDC_EDIT1                       14000
#define IDC_EDIT2                       14001
#define IDC_EDIT3                       14002
#define IDC_EDIT4                       14003
#define IDI_FORMICON                    14004
#define IDC_EDIT5                       14004
#define IDR_TOOLBAR1                    14005
#define IDC_EDIT6                       14005
#define IDC_LBL1                        14006
#define IDC_LBL2                        14007
#define IDD_DIALOG1                     14007
#define IDC_LBL3                        14008
#define IDC_LBL4                        14009
#define IDC_LBL5                        14010
#define IDB_BITMAP1                     14010
#define IDB_TAB_ICONS                   14010
#define IDC_LBL6                        14011
#define IDC_GRP1                        14012
#define IDD_DIALOG2                     14012
#define IDC_BUTTON1                     14013
#define IDC_EDIT7                       14013
#define IDC_BUTTON2                     14014
#define IDC_LBL7                        14014
#define IDB_OPENPROP                    14014
#define IDC_EDIT8                       14015
#define IDD_DIALOG3                     14015
#define IDC_LBL8                        14016
#define IDR_TOOLBAR2                    14016
#define IDC_LBL9                        14017
#define IDB_BITMAP2                     14017
#define IDC_LIST2                       14018
#define IDC_EDIT9                       14018
#define IDC_CUSTOM1                     14019
#define IDC_LBL2_1                      14020
#define IDC_EDIT2_1                     14021
#define IDC_LBL2_2                      14022
#define IDC_EDIT2_2                     14023
#define IDC_BUTTON3                     14024
#define IDC_BTN_OPENPROP                14024
#define IDC_LBL_1                       14025
#define IDC_LBL_NUMOF                   14025
#define IDC_EDIT_NUMOF                  14026
#define IDC_LBL10                       14027
#define IDC_EDIT10                      14028
#define ID_TBTN_EXPFILE                 32771
#define ID_TBTN_IMPTREE                 32772
#define ID_TBTN_INCSPC                  32773
#define ID_TBTN_DECSPC                  32774
#define ID_TBTN_SETTINGS                32775
#define ID_TBTN_GENERAL                 32776
#define ID_TBTN_PREVIEW                 32779
#define ID_BTN_PREVIEW_PRINT            32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        14018
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         14029
#define _APS_NEXT_SYMED_VALUE           14000
#endif
#endif
