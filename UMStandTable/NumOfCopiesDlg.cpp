// NumOfCopiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NumOfCopiesDlg.h"


// CNumOfCopiesDlg dialog

IMPLEMENT_DYNAMIC(CNumOfCopiesDlg, CDialog)

CNumOfCopiesDlg::CNumOfCopiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNumOfCopiesDlg::IDD, pParent)
	
{
	m_nNum;
}

CNumOfCopiesDlg::~CNumOfCopiesDlg()
{
}

void CNumOfCopiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_NUMOF, m_wndEdit);
	DDX_Control(pDX, IDC_LBL_NUMOF, m_wndLbl);
}


BEGIN_MESSAGE_MAP(CNumOfCopiesDlg, CDialog)
	ON_EN_CHANGE(IDC_EDIT_NUMOF, &CNumOfCopiesDlg::OnEnChangeEditNumof)
END_MESSAGE_MAP()


// CNumOfCopiesDlg message handlers

BOOL CNumOfCopiesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	setLanguage();
	
	m_wndEdit.SetAsNumeric();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CNumOfCopiesDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			SetWindowText(xml->str(IDS_STRING186));
			m_wndLbl.SetWindowTextW(xml->str(IDS_STRING187));
		}
		delete xml;
	}
}

void CNumOfCopiesDlg::OnEnChangeEditNumof()
{
	m_nNum = _tstoi(m_wndEdit.getText());
}
