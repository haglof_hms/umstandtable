#include "StdAfx.h"
#include "UMStandTableDB.h"

CUMStandTableDB::CUMStandTableDB(void)
{
}

CUMStandTableDB::~CUMStandTableDB(void)
{
}

CUMStandTableDB::CUMStandTableDB(DB_CONNECTION_DATA &conn)
{
	m_Cmd.setConnection(conn.conn);
}

BOOL CUMStandTableDB::getSpecieDataFromDB(vecTransactionSpecies &vec)
{
	int nCnt;

	try
	{
		vec.clear();
		m_Cmd.setCommandText(_T("select * from fst_species_table order by spc_id"));
		m_Cmd.Execute();

		while(m_Cmd.FetchNext())
		{
			vec.push_back(CTransaction_species(nCnt,
					m_Cmd.Field(1).asLong(),
					m_Cmd.Field(_T("p30_spec")).asShort(),
					m_Cmd.Field(2).asString(),
					m_Cmd.Field(3).asString(),
					m_Cmd.Field(4).asString()));
			nCnt++;
		}
		m_Cmd.Close();
	}
	catch(SAException )//&e)
	{
		//AfxMessageBox(e.ErrText());
		return FALSE;
	}

	return TRUE;
}

BOOL CUMStandTableDB::getPropertyDataFromDB(vecTransactionProperty &vec, LPCTSTR quest)
{
	try
	{
		vec.clear();
		
		m_Cmd.setCommandText((SAString)quest);
		m_Cmd.Execute();
		
		while(m_Cmd.FetchNext())
		{
			vec.push_back(CTransaction_property(m_Cmd.Field(1).asLong(),
				(LPCTSTR)m_Cmd.Field(2).asString(),
				(LPCTSTR)m_Cmd.Field(3).asString(),
				(LPCTSTR)m_Cmd.Field(4).asString(),
				(LPCTSTR)m_Cmd.Field(5).asString(),
				(LPCTSTR)m_Cmd.Field(6).asString(),
				(LPCTSTR)m_Cmd.Field(7).asString(),
				(LPCTSTR)m_Cmd.Field(8).asString(),
				(LPCTSTR)m_Cmd.Field(9).asString(),
				(LPCTSTR)m_Cmd.Field(10).asString(),
				(LPCTSTR)m_Cmd.Field(11).asString(),
				m_Cmd.Field(12).asDouble(),
				m_Cmd.Field(13).asDouble(),
				(LPCTSTR)m_Cmd.Field(14).asString(),
				(LPCTSTR)m_Cmd.Field(15).asString(),
				(LPCTSTR)m_Cmd.Field(16).asString(),
				(LPCTSTR)m_Cmd.Field(17).asString(),
				(LPCTSTR)m_Cmd.Field(18).asString() ));
		}
		m_Cmd.Close();
	}
	catch(SAException &)
	{
		 // print error message
//		AfxMessageBox((LPCWSTR)e.ErrText());
		return FALSE;
	}

	return TRUE;
}

