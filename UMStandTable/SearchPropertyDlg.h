#pragma once

#include "Resource.h"

#include "UMStandTableDB.h"
/////////////////////////////////////////////////////////////////////////////////
// CSelectedPropertiesDataRec

class CSelectedPropertiesDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_property propRec;
	CString sPropName;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CSelectedPropertiesDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CSelectedPropertiesDataRec(UINT index,CTransaction_property data)	 
	{
		m_nIndex = index;
		propRec = data;
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
		AddItem(new CTextItem((data.getPropertyNum())));
		sPropName.Format(_T("%s %s:%s"),
			data.getPropertyName(),
			data.getBlock(),
			data.getUnit());
		AddItem(new CTextItem((sPropName)));
		
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
	CTransaction_property &getRecord(void)
	{
		return propRec;
	}

};

// CSearchPropertyDlg dialog

class CSearchPropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CSearchPropertyDlg)
	// Setup language filename; 051214 p�d
	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_csDBErrorMsg;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;

	CButton m_wndSearchBtn;
	CButton m_wndClearBtn;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	void setupReport(void);

	void setupSQLFromSelections(void);

	CTransaction_property m_recSelProp;
	vecTransactionProperty m_vecTransactionProperty;
	
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

public:
	CSearchPropertyDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSearchPropertyDlg();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR2 };
	
	CTransaction_property &getSelectedProperty(void)
	{
		return m_recSelProp;
	}

protected:
	//{{AFX_VIRTUAL(CSearchPropertyDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	//{{AFX_MSG(CSearchPropertyDlg)
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnBnClickedBtnSearch();
	afx_msg void OnBnClickedBtnClear();
	afx_msg void OnBnClickedBtnOK();
};
