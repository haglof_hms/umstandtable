#pragma once
#include "stdafx.h"

class CSampleTreeViewReportRec : public CXTPReportRecord
{

protected:
	class CFloatItem : public CXTPReportRecordItemNumber
	{
		double m_fValue;
	public:
		CFloatItem(double fValue, LPCTSTR fmt_str = sz1dec) : CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		double getFloatItem(void) { return m_fValue;}

		void setFloatItem(double value)
		{
			m_fValue = value;
			SetValue(value);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
		}
	};

	class CIntItem : public CXTPReportRecordItemNumber
	{
		int m_nValue;

	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)
		{
			m_nValue = nValue;
		}

		int getIntItem(void) { return m_nValue;}

		void setIntItem(int nValue)
		{
			m_nValue = nValue;
			SetValue(nValue);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
		}

	};

	class CTextItem : public CXTPReportRecordItemText
	{
		CString m_sText;

	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}

		CString getTextItem(void) { return m_sText;}

		void setTextItem(LPCTSTR sValue)
		{
			m_sText = sValue;
			SetValue(m_sText);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR sValue)
		{
			m_sText = sValue;
			SetValue(sValue);
		}
	};

public:
	CSampleTreeViewReportRec(void)
	{
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CTextItem(_T("")));
	}

	CSampleTreeViewReportRec(LPCTSTR type)
	{
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CFloatItem(0,sz0dec));
		AddItem(new CTextItem(type));
	}

	CSampleTreeViewReportRec(CMyTransactionSampleTree *rec)
	{
		AddItem(new CFloatItem(rec->getDbh(),sz0dec));
		AddItem(new CFloatItem(rec->getHgt(),sz0dec));
		AddItem(new CFloatItem(rec->getGCrown(),sz0dec));
		AddItem(new CFloatItem(rec->getBrk(),sz0dec));
		AddItem(new CFloatItem(rec->getAge(),sz0dec));
		AddItem(new CFloatItem(rec->getBonitet(),sz0dec));
		AddItem(new CTextItem(rec->getTypeName()));
	}

	CSampleTreeViewReportRec(CSampleTreeViewReportRec *rec)
	{
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_DBH),sz0dec));
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_HGT),sz0dec));
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_GCROWN),sz0dec));
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_BARK),sz0dec));
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_AGE),sz0dec));
		AddItem(new CFloatItem(rec->getColumnFloat(ST_COL_BONITET),sz0dec));
		AddItem(new CTextItem(rec->getColumnText(ST_COL_TYPE)));
	}

	double getColumnFloat(int item)
	{
		return ((CFloatItem*)GetItem(item))->getFloatItem();
	}

	void setColumnFloat(int item, double value)
	{
		((CFloatItem*)GetItem(item))->setFloatItem(value);
	}

	int getColumnInt(int item)
	{
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	void setColumnInt(int item, int value)
	{
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

	CString getColumnText(int item)
	{
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	void setColumnText(int item, LPCTSTR value)
	{
		((CTextItem*)GetItem(item))->setTextItem(value);
	}


};
