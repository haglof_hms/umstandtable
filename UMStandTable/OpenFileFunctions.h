#pragma once

// Utility dlls
const LPCTSTR STANFORDDLL_FILENAME		= _T("Stanford.dll");
const LPCTSTR HXLDLL_FILENAME			= _T("XMLReader.dll");

// Utility function pointers
typedef int (WINAPI *Func_FileOpen)(char *filename);
typedef int (WINAPI *Func_GetVersion)(char str[]);
typedef int (WINAPI *Func_GetNumOfSet)(int *variabel);
typedef int (WINAPI *Func_GetInteger)(int part, int var, int type, int index,int *variabel);
typedef int (WINAPI *Func_GetText)(int part, int var, int type, int index, char str[]);

// Stanford function names
const LPCSTR STANFORDFUNC_FILEOPEN		= "StanfordFileOpen";
const LPCSTR STANFORDFUNC_GETVERSION	= "StanfordGetVersion";
const LPCSTR STANFORDFUNC_GETNUMOFSET	= "StanfordGetNumOfSet";
const LPCSTR STANFORDFUNC_GETINTEGER	= "StanfordGetInteger";
const LPCSTR STANFORDFUNC_GETTEXT		= "StanfordGetText";

// XMLReader function names
const LPCSTR HXLFUNC_FILEOPEN			= "HXL_FileOpen";
const LPCSTR HXLFUNC_GETVERSION			= "HXL_GetVersion";
const LPCSTR HXLFUNC_GETNUMOFSET		= "HXL_GetNumOfSet";
const LPCSTR HXLFUNC_GETINTEGER			= "HXL_GetInteger";
const LPCSTR HXLFUNC_GETTEXT			= "HXL_GetText";

// File type indicators
const int FILETYPE_HXL = 1;
const int FILETYPE_INV = 2;


class COpenFileFunctions
{
protected:
	HINSTANCE m_hInstReader;		// Reader utility dll instance
	int m_nFileType;				// Inventory file type (hxl/inv)
	SPECNAMES m_SpecNames[NUM_OF_MSPEC];
	vecMySampleTrees m_vecTrees;
	vecMySampleTrees m_vecSampleTrees;
	vecMySampleTrees m_vecRandTrees;

public:
	COpenFileFunctions();
	~COpenFileFunctions();

	BOOL readDataFromFile(void);

protected:
	int OpenFile(LPCTSTR fn);

	// Helper funcs
	CString getDllDirectory();
	CString getStanfordDllPath();
	CString getHXLDllPath();

	int GetNumOfSet();
	int GetIntData(int set, int var, int type, int index);
	TCHAR *GetTextData(int set, int var, int type, int index);
	BOOL extractData(int set);
	void convertSi(int nSi, CString &sSi);
	void getTreeDataInv(int set);
	void getTreeDataHxl(int set);
	double getDcls(void);
	int getSpecNameFromFile(int set);
	int getTreesFromFile(int set);
	void setSpecUsed(int id);
};
