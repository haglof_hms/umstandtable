// AddDclsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AddDclsDlg.h"


// CAddDclsDlg dialog

IMPLEMENT_DYNAMIC(CAddDclsDlg, CDialog)

CAddDclsDlg::CAddDclsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddDclsDlg::IDD, pParent)
{

}

CAddDclsDlg::~CAddDclsDlg()
{
}

void CAddDclsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);

	DDX_Control(pDX, IDC_LBL2_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2_2, m_wndLbl2);

	DDX_Control(pDX, IDC_EDIT2_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2_2, m_wndEdit2);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
}


BEGIN_MESSAGE_MAP(CAddDclsDlg, CDialog)
END_MESSAGE_MAP()


// CAddDclsDlg message handlers

BOOL CAddDclsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPSIBLINGS|WS_CLIPCHILDREN;

	return TRUE;
}

BOOL CAddDclsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLang.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, m_sAbrevLangSet, LANGUAGE_FN_EXT);

	setLanguage();

	m_wndEdit1.SetEnabledColor(BLACK, WHITE);
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK);
	m_wndEdit2.SetEnabledColor(BLACK, WHITE);
	m_wndEdit2.SetDisabledColor(BLACK, INFOBK);

	m_wndEdit1.SetAsNumeric();
	m_wndEdit2.SetAsNumeric();

	return TRUE;
}

void CAddDclsDlg::setLanguage(void)
{
	if(fileExists(m_sLang))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLang))
		{
			SetWindowText(xml->str(IDS_STRING113));
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING111));
			m_wndLbl2.SetWindowText(xml->str(IDS_STRING142));
			m_wndBtnOK.SetWindowText(xml->str(IDS_STRING135));
			m_wndBtnCancel.SetWindowText(xml->str(IDS_STRING136));
		}
		delete xml;
	}
}

