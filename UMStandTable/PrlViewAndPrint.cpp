// PrlViewAndPrint.cpp : implementation file
//

#include "stdafx.h"
#include "PrlViewAndPrint.h"
#include "ResLangFileReader.h"
#include "MDIStandTableFrame.h"
#include "StandTableView.h"
#include "SpeciesView.h"
#include "SpeciesViewReportRec.h"
#include "SampleTreeView.h"
#include "SampleTreeViewReportRec.h"
#include "SampleTreeTabPageManager.h"


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc

IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc construction/destruction

CMDIPrlViewAndPrintDoc::CMDIPrlViewAndPrintDoc()
{
}

CMDIPrlViewAndPrintDoc::~CMDIPrlViewAndPrintDoc()
{
}

BOOL CMDIPrlViewAndPrintDoc::OnNewDocument()
{

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc serialization

void CMDIPrlViewAndPrintDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintDoc diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIPrlViewAndPrintDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIPrlViewAndPrintDoc commands

/////////////////////////////////////////////////////////////////////////////
// CMDIPrlViewAndPrintFrame


IMPLEMENT_DYNCREATE(CMDIPrlViewAndPrintFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPrlViewAndPrintFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPrlViewAndPrintFrame)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND(ID_BTN_PREVIEW_PRINT, OnPrintOut)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPrlViewAndPrintFrame::CMDIPrlViewAndPrintFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bIsSettingsTBtn = TRUE;
}

CMDIPrlViewAndPrintFrame::~CMDIPrlViewAndPrintFrame()
{
}

void CMDIPrlViewAndPrintFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_STANDTABLE_PREVIEW_LAYOUT_KEY);
	SavePlacement(this, csBuf);

	//remove temp file
	if(fileExists(PRL_VIEW_FN))
		CFile::Remove(PRL_VIEW_FN);
}

int CMDIPrlViewAndPrintFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	CString csToolTip1;

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			csToolTip1 = xml.str(IDS_STRING189);
		}
		xml.clean();
	}

	//Create and Load toolbar
	
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString csResFN = getToolBarResFN();

	//set toolbar icon
	if(fileExists(csResFN))
	{
		CXTPToolBar *pToolBar = &m_wndToolBar;
		if(pToolBar->IsBuiltIn())
		{
			if(pToolBar->GetType() != xtpBarTypeMenuBar)
			{
				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				if(nBarID == IDR_TOOLBAR2)
				{

					pCtrl = p->GetAt(0);
					pCtrl->SetTooltip(csToolTip1);
					hIcon = ExtractIcon(AfxGetInstanceHandle(), csResFN, RSTR_TB_PRINT);
					if(hIcon)
						pCtrl->SetCustomIcon(hIcon);
				}
			}
		}
	}

	setLanguage();

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPrlViewAndPrintFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


// CMDIPrlViewAndPrintFrame diagnostics

#ifdef _DEBUG
void CMDIPrlViewAndPrintFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPrlViewAndPrintFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


// PRIVATE
void CMDIPrlViewAndPrintFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

// load the placement in OnShowWindow()
void CMDIPrlViewAndPrintFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_STANDTABLE_PREVIEW_LAYOUT_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPrlViewAndPrintFrame::OnSetFocus(CWnd *wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CMDIChildWnd::OnSetFocus(wnd);

}

void CMDIPrlViewAndPrintFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PREVIEW;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PREVIEW;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPrlViewAndPrintFrame::OnPrintOut(void)
{

	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW3);
	if (pPrlView != NULL)
	{
		pPrlView->printOut();
		pPrlView = NULL;
	}

}

void CMDIPrlViewAndPrintFrame::OnSettings(void)
{
/*
	CPrlViewAndPrint *pPrlView = (CPrlViewAndPrint*)getFormViewByID(IDD_FORMVIEW3);
	if (pPrlView != NULL)
	{
		pPrlView->settingsDlg();
		pPrlView = NULL;
	}
*/
}

void CMDIPrlViewAndPrintFrame::OnSettingsTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsSettingsTBtn );
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPrlViewAndPrintFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}

// MY METHODS
void CMDIPrlViewAndPrintFrame::setLanguage()
{
}

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint

IMPLEMENT_DYNCREATE(CPrlViewAndPrint, CMyHtmlView)

BEGIN_MESSAGE_MAP(CPrlViewAndPrint, CMyHtmlView)
	//{{AFX_MSG_MAP(CPrlViewAndPrint)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPrlViewAndPrint::CPrlViewAndPrint()
{
	//{{AFX_DATA_INIT(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CPrlViewAndPrint::~CPrlViewAndPrint()
{
}

void CPrlViewAndPrint::DoDataExchange(CDataExchange* pDX)
{
	CMyHtmlView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrlViewAndPrint)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint diagnostics

#ifdef _DEBUG
void CPrlViewAndPrint::AssertValid() const
{
	CMyHtmlView::AssertValid();
}

void CPrlViewAndPrint::Dump(CDumpContext& dc) const
{
	CMyHtmlView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrlViewAndPrint message handlers

void CPrlViewAndPrint::OnInitialUpdate()
{
	setHTMFileName(PRL_VIEW_FN);
	setupHTM();
}

void CPrlViewAndPrint::setupHTM()
{
	int nMaxGroups, nGroup, nSumDcls, nTotDcls,res;
	int *arrDcls;
	CString csNum, csDcls, csTmp;
	vecMySampleTrees vecST;
	CMyTransactionSampleTree SampleTree;

	CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
	if(!pFrameWnd)
		return;

	CMDIStandTableFrame *pWnd = (CMDIStandTableFrame*)pFrameWnd->GetActiveFrame();
	if(!pWnd)
		return;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	RLFReader xml;
	
	if (fileExists(m_sLangFN))
	{
		if (xml.Load(m_sLangFN))
		{
			//Start of HTML-file
			startHTM();

			setHTM_text(getDBDateTime(),HTM_FREE_FMT,1);
			setHTML_linefeed(1);

			setHTM_text(xml.str(IDS_STRING100),HTM_FREE_FMT,4,_T(""),HTM_COLOR_BLACK, true);
			setHTML_linefeed(1);
			
			//property
			setHTM_text(xml.str(IDS_STRING103),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getName(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//block
			setHTM_text(xml.str(IDS_STRING101),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getNumber(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//unit
			setHTM_text(xml.str(IDS_STRING102),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getSubNumber(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//del nummer
			setHTM_text(xml.str(IDS_STRING191),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getDelnr(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//area
			setHTM_text(xml.str(IDS_STRING104),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getArea(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//latitude
			setHTM_text(xml.str(IDS_STRING105),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getLatitude(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//altitude
			setHTM_text(xml.str(IDS_STRING106),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getHeightOverSea(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//age
			setHTM_text(xml.str(IDS_STRING107),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getAge(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			//SI
			setHTM_text(xml.str(IDS_STRING108),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getH100(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);
			
			//Side 1 or 2
			setHTM_text(xml.str(IDS_STRING109) + _T(":"),HTM_FREE_FMT,2);
			setHTM_text(pWnd->m_wndGeneralDataDlg.getSide(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
			setHTML_linefeed(1);

			setHTML_line(HTM_HR_NOSHADE,2);

			CStandTableView *pView = (CStandTableView*)getFormViewByID(IDD_FORMVIEW);
			if(pView)
			{
				pView->updateRecords();	//update records to get recent changes

				CMyTabControl *pTab = &pView->m_wndTabControl;
				if(pTab)
				{
					
					for(int i=0; i<pTab->getNumOfTabPages(); i++)	//uttag, kvarl�mnat, uttag stickv�g, provtr�d
					{
						CXTPTabManagerItem *pItem = pTab->getTabPage(i);

						if(pItem)
						{
							if(i != TAB_SAMPLETREE)	//ej f�r kanttr�den
							{
								CTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

								if(pTabManager)
								{
									res = 0;
									for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)	//f�r varje tr�dslag
									{
										CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(j);
										if(pManager)
										{
											CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pManager->GetHandle()));
											if(!pSpcView)
												continue;

											res += pSpcView->getNumOfTrees();
										}
									}//for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)

									//skriv inte ut om det inte finns n�gra tr�d
									if(res == 0)
										continue;

									//�verskrift
									setHTM_text(pItem->GetCaption(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
									setHTML_linefeed(1);


									nMaxGroups = 0;

									// Start by setting up the column widths
									setHTM_table();
									setHTM_start_table_column();
									setHTM_table_column(_T(""),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);


									//hitta st�rst antal diameterklasser
									for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)	//f�r varje tr�dslag
									{
										CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(j);
										if(pManager)
										{
											setHTM_table_column(pManager->GetTooltip(),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);	//tr�dslagsnamnen

											CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pManager->GetHandle()));
											if(!pSpcView)
												continue;

											CXTPReportRecords *pRecs = pSpcView->m_wndReport.GetRecords();
											if(!pRecs)
												continue;

											if(pRecs->GetCount() > nMaxGroups)
												nMaxGroups = pRecs->GetCount();
										}
									}//for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)

									setHTM_table_column(xml.str(IDS_STRING190),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_end_table_column();

									//allokera minne f�r rad array
									arrDcls = (int *)calloc(pTabManager->m_wndTabControl.getNumOfTabPages(), sizeof(int));

									if(arrDcls == NULL)
									{
										GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
										return;
									}

									nMaxGroups--; //r�kna ej med sista raden

									//f�r varje diameterklass och tr�dslag
									for(int k=0; k<nMaxGroups; k++)
									{
										//nollst�ll array
										for(int l=0; l<pTabManager->m_wndTabControl.getNumOfTabPages(); l++)	//kolla om raden bara inneh�ller 0:or
										{
											arrDcls[l] = 0;
										}

										//h�mta in diameterklassrad
										for(int m=0; m<pTabManager->m_wndTabControl.getNumOfTabPages(); m++)	//f�r varje tr�dslag
										{
											CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(m);
											if(pManager)
											{
												CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pManager->GetHandle()));
												if(!pSpcView)
													continue;

												CXTPReportRecords *pRecs = pSpcView->m_wndReport.GetRecords();
												if(!pRecs)
													continue;

												if(k < pRecs->GetCount()-1)
												{
													CSpeciesViewReportRec *pSpcRec = (CSpeciesViewReportRec*)pRecs->GetAt(k);
													if(!pSpcRec)
														continue;

													arrDcls[m] = (int)pSpcRec->getColumnFloat(2);	
												}
											}//if(pManager)
										}//for(int m=0; m<pTabManager->m_wndTabControl.getNumOfTabPages(); m++)

										//kolla om raden bara inneh�ller 0:or
										res = 0;
										for(int l=0; l<pTabManager->m_wndTabControl.getNumOfTabPages(); l++)	
										{
											res += arrDcls[l];
										}

										if(res > 0)
										{
											//tr�d finns, skriv ut diameterklass
											setHTM_start_table_column();

											nGroup = k * (int)pWnd->m_wndSettingsDataDlg.getDcls();
											csDcls.Format(_T("%d"), nGroup);
											setHTM_table_column(csDcls,100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);

											for(int n=0; n<pTabManager->m_wndTabControl.getNumOfTabPages(); n++)	
											{
												csNum.Format(_T("%d"),arrDcls[n]);
												setHTM_table_column(csNum,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
											}

											//totalt diameterklass
											csNum.Format(_T("%d"),res);
											setHTM_table_column(csNum,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
											setHTM_end_table_column();
										}

									}//for(int k=0; k<nMaxGroups; k++)


									//summera antal tr�d per tr�dslag
									nSumDcls = 0;
									nTotDcls = 0;

									setHTM_start_table_column();
									setHTM_table_column(xml.str(IDS_STRING190),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);

									for(int n=0; n<pTabManager->m_wndTabControl.getNumOfTabPages(); n++)	//f�r varje tr�dslag
									{
										CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(n);
										if(pManager)
										{
											CSpeciesView *pSpcView = DYNAMIC_DOWNCAST(CSpeciesView, CWnd::FromHandle(pManager->GetHandle()));
											if(!pSpcView)
												continue;

											nSumDcls = pSpcView->getNumOfTrees();
											nTotDcls += nSumDcls;

											csNum.Format(_T("%d"),nSumDcls);
											setHTM_table_column(csNum,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
										}
									}

									//Totalt
									csNum.Format(_T("%d"),nTotDcls);
									setHTM_table_column(csNum,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);

									endHTM_table();
									setHTML_linefeed(1);

									//avallokera array
									free(arrDcls);
								}//if(pTabManager)
							}//if(i != TAB_SAMPLETREE)
							else if(i == TAB_SAMPLETREE)
							{
								CSampleTreeTabPageManager *pTabManager = DYNAMIC_DOWNCAST(CSampleTreeTabPageManager,CWnd::FromHandle(pItem->GetHandle()));

								if(pTabManager)
								{
									//skriv bara ut om provtr�d finns 
									res = 0;
									for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)
									{
										CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(j);
										if(pManager)
										{
											CSampleTreeView *pSTView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pManager->GetHandle()));
											if(!pSTView)
												continue;

											res += pSTView->getNumOfTrees();
										}
									}

									if(res == 0)
										continue;

									//�verskrift
									setHTM_text(pItem->GetCaption(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLACK,true);
									setHTML_linefeed(1);

									setHTM_table();
									setHTM_start_table_column();
									setHTM_table_column(_T(""),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING143),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING144),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING145),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING146),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING147),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING148),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_table_column(xml.str(IDS_STRING134),100,2,_T(""),HTM_COLOR_BLUE,HTM_ALIGN_LEFT,false,false,true);
									setHTM_end_table_column();
									endHTM_table();

									for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)
									{
										CXTPTabManagerItem *pManager = pTabManager->m_wndTabControl.getTabPage(j);
										if(pManager)
										{
											setHTM_text(pManager->GetTooltip(),HTM_FREE_FMT,2,_T(""),HTM_COLOR_BLUE,false);
											setHTML_linefeed(1);

											CSampleTreeView *pSTView = DYNAMIC_DOWNCAST(CSampleTreeView, CWnd::FromHandle(pManager->GetHandle()));
											if(!pSTView)
												continue;

											vecST.clear();
											//varning om provtr�d/kanttr�d saknar v�rden
											if(pSTView->getSampleTree(vecST, (int)pManager->GetData()) == FALSE)
											{
												//avsluta
												GetParentFrame()->PostMessage(WM_COMMAND, ID_FILE_CLOSE);
												return;
											}

											if((int)vecST.size() > 0)
											{
												//per tr�dslag
												setHTM_table();

												//f�r varje provtr�d
												for(int k=0; k<(int)vecST.size(); k++)
												{
													SampleTree = vecST[k];

													setHTM_start_table_column();
													setHTM_table_column(_T(""),100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getDbh());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getHgt());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getGCrown());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getBrk());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getAge());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%.0f"),SampleTree.getBonitet());
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);
													csTmp.Format(_T("%s"),pSTView->m_sarrTreeTypes[SampleTree.getType()]);
													setHTM_table_column(csTmp,100,2,_T(""),HTM_COLOR_BLACK,HTM_ALIGN_LEFT,false,false,false);

													setHTM_end_table_column();
												}//for(int k=0; k<(int)vecST.size(); k++)
												endHTM_table();
											}//if((int)vecST.size() > 0)
											setHTML_linefeed(1);
										}//if(pManager)
									}//for(int j=0; j<pTabManager->m_wndTabControl.getNumOfTabPages(); j++)
									setHTML_line(HTM_HR_NOSHADE,2);
								}//if(pTabManager)
							}//else if(i == TAB_SAMPLETREE)							
						}//if(pItem)
					}//for(int i=0; i<pTab->getNumOfTabPages(); i++)
				}//if(pTab)
			}//if(pView)

		}
		xml.clean();
	}

	// End of HTML-file
	endHTM();

	// Save to disk and load into Viewer; 090901 p�d
	showHTM();
}

void CPrlViewAndPrint::printOut()
{
	printOutHTM();
}


