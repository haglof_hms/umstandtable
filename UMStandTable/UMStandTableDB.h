#pragma once
#include "stdafx.h"

class CUMStandTableDB
{
	SACommand m_Cmd;
	DB_CONNECTION_DATA data;
public:
	CUMStandTableDB(DB_CONNECTION_DATA &conn);
	CUMStandTableDB(void);
	~CUMStandTableDB(void);

	BOOL getSpecieDataFromDB(vecTransactionSpecies &vec);
	BOOL getPropertyDataFromDB(vecTransactionProperty &vec, LPCTSTR quest);

};
