// SearchPropertyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UMStandTableDB.h"
#include "SearchPropertyDlg.h"

// CSearchPropertyDlg dialog

IMPLEMENT_DYNAMIC(CSearchPropertyDlg, CDialog)

BEGIN_MESSAGE_MAP(CSearchPropertyDlg, CDialog)
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, IDC_SEL_PROP_REPORT, OnReportItemClick)
	ON_BN_CLICKED(IDC_BUTTON1, &CSearchPropertyDlg::OnBnClickedBtnSearch)
	ON_BN_CLICKED(IDC_BUTTON2, &CSearchPropertyDlg::OnBnClickedBtnClear)
	ON_BN_CLICKED(IDOK, &CSearchPropertyDlg::OnBnClickedBtnOK)
END_MESSAGE_MAP()

CSearchPropertyDlg::CSearchPropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSearchPropertyDlg::IDD, pParent)
{
	m_bConnected = FALSE;
}

CSearchPropertyDlg::~CSearchPropertyDlg()
{
	m_vecTransactionProperty.clear();
}

void CSearchPropertyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)
	DDX_Control(pDX, IDC_LBL_DLGBAR1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL_DLGBAR2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL_DLGBAR3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL_DLGBAR4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL_DLGBAR5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL_DLGBAR6, m_wndLbl6);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT17, m_wndEdit6);

	DDX_Control(pDX, IDC_BUTTON1, m_wndSearchBtn);
	DDX_Control(pDX, IDC_BUTTON2, m_wndClearBtn);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP
}

BOOL CSearchPropertyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	
	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd());


	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			SetWindowText((xml->str(IDS_STRING180)));

			m_wndLbl1.SetWindowText((xml->str(IDS_STRING170)));
			m_wndLbl2.SetWindowText((xml->str(IDS_STRING171)));
			m_wndLbl3.SetWindowText((xml->str(IDS_STRING172)));
			m_wndLbl4.SetWindowText((xml->str(IDS_STRING173)));
			m_wndLbl5.SetWindowText((xml->str(IDS_STRING174)));
			m_wndLbl6.SetWindowText((xml->str(IDS_STRING175)));

			m_wndSearchBtn.SetWindowText((xml->str(IDS_STRING176)));
			m_wndClearBtn.SetWindowText((xml->str(IDS_STRING177)));
			m_wndOKBtn.SetWindowText((xml->str(IDS_STRING178)));
			m_wndCancelBtn.SetWindowText((xml->str(IDS_STRING179)));
			
			m_csDBErrorMsg = xml->str(IDS_STRING167);
		}
		delete xml;
	}

	m_wndOKBtn.EnableWindow( FALSE );	// Only enable on selected item; 080429 p�d

	setupReport();
	setupSQLFromSelections();

	return TRUE;
}

BOOL CSearchPropertyDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	if(pCopyDataStruct->cbData == sizeof(DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData, pCopyDataStruct->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
	}

	return CDialog::OnCopyData(pWnd, pCopyDataStruct);
}

// PRIVATE
void CSearchPropertyDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,IDC_SEL_PROP_REPORT, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	
		/*VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));

		m_wndReport.SetImageList(&m_ilIcons);
*/
		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if (xml->Load(m_sLangFN))
			{
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COL_PROP_0, (xml->str(IDS_STRING170)), 150));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COL_PROP_1, (xml->str(IDS_STRING171)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COL_PROP_2, (xml->str(IDS_STRING172)), 120));
				pCol->SetEditable( FALSE );

				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COL_PROP_3, (xml->str(IDS_STRING173)), 200));
				pCol->SetEditable( FALSE );
/*
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING2315)), 200));
				pCol->SetEditable( FALSE );
*/
				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_SEL_PROP_REPORT),5,80,rect.right - 10,rect.bottom - 110);
			}
			delete xml;
		}	// if (fileExists(sLangFN))

	}
}

void CSearchPropertyDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (m_wndReport.GetFocusedRow())
			m_wndOKBtn.EnableWindow(TRUE);
	}
}

void CSearchPropertyDlg::setupSQLFromSelections(void)
{
	
	CStringArray arrSQL;
	CString sSQL;
	CString sCounty = _T("");
	CString sMunicipal = _T("");
	CString sRegion = _T("");
	CString sNumber = _T("");
	CString sName = _T("");
	CString sBlock = _T("");
	CString sUnit = _T("");

	arrSQL.RemoveAll();
	// Try to find out how many items's entered;
	if (!m_wndEdit1.getText().IsEmpty())
	{
		sCounty.Format(_T("county_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit1.getText(),_T("%"));
		arrSQL.Add(sCounty);
	}
	if (!m_wndEdit2.getText().IsEmpty())
	{
		sMunicipal.Format(_T("municipal_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit2.getText(),_T("%"));
		arrSQL.Add(sMunicipal);
	}
	if (!m_wndEdit3.getText().IsEmpty())
	{
		sNumber.Format(_T("prop_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit3.getText(),_T("%"));
		arrSQL.Add(sNumber);
	}
	if (!m_wndEdit4.getText().IsEmpty())
	{
		sName.Format(_T("prop_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit4.getText(),_T("%"));
		arrSQL.Add(sName);
	}
	if (!m_wndEdit5.getText().IsEmpty())
	{
		sBlock.Format(_T("block_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit5.getText(),_T("%"));
		arrSQL.Add(sBlock);
	}
	if (!m_wndEdit6.getText().IsEmpty())
	{
		sBlock.Format(_T("unit_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit6.getText(),_T("%"));
		arrSQL.Add(sBlock);
	}

	if (arrSQL.GetCount() > 0)
	{
		sSQL.Format(_T("select * from %s where "),TBL_PROPERTY);
		// How many items in array; 080122 p�d
		for (int i = 0;i < arrSQL.GetCount();i++)
		{
			if (i > 0)
				sSQL += _T(" and ");

			sSQL += arrSQL.GetAt(i);
		}
	}
	else
		sSQL.Format(_T("select * from %s"),TBL_PROPERTY);	// Select all properties; 080122 p�d
	// Clear report for next question
	m_wndReport.ResetContent();

	if(m_bConnected)
	{
		getPropertiesFromDB(m_dbConnectionData, m_vecTransactionProperty, sSQL);

		// Do collecting data from Database; 080122 p�d
		//	if (m_pDB != NULL)
		//	{
		//		m_pDB->getProperties(sSQL,m_vecTransactionProperty);

		// Disable OK button if there's no items in list; 080429 p�d
		m_wndOKBtn.EnableWindow( m_vecTransactionProperty.size() > 0 );

		if (m_vecTransactionProperty.size() > 0)
		{
			// Setup data in report; 080122 p�d
			for (UINT ii = 0;ii < m_vecTransactionProperty.size();ii++)
			{
				m_wndReport.AddRecord(new CSelectedPropertiesDataRec(ii,m_vecTransactionProperty[ii]));
			}	// for (UINT ii = 0;ii < m_vecTransactionProperty.size();ii++)
		}	// if (m_vecTransactionProperty.size() > 0)
	}	// if (m_pDB != NULL)
	else
	{
		//error or no database connection
		AfxMessageBox(m_csDBErrorMsg, MB_ICONEXCLAMATION);
		OnCancel();
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
	
}


void CSearchPropertyDlg::OnBnClickedBtnSearch()
{
	setupSQLFromSelections();
}

void CSearchPropertyDlg::OnBnClickedBtnClear()
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
/*	m_wndReport.ResetContent();
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
*/
	setupSQLFromSelections();

	m_wndEdit1.SetFocus();
}

void CSearchPropertyDlg::OnBnClickedBtnOK()
{
	CXTPReportRow *pRow = m_wndReport.GetFocusedRow();
	if (pRow != NULL)
	{
		CSelectedPropertiesDataRec *pRec = (CSelectedPropertiesDataRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_recSelProp = pRec->getRecord();
		}
		OnOK();
	}
}
