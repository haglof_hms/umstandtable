#pragma once
#include "Resource.h"
#include "BPCtrlAnchorMap.h"
#include "OXMaskedEdit.h"

// CGeneralDataDlg dialog

class CGeneralDataDlg : public CDialog
{
	DECLARE_DYNAMIC(CGeneralDataDlg)

	CString m_sLangFN;
	CString m_sAbrevLangSet;

	CString m_csMsgSide;
	CString m_csMsgSILimit;
	CString m_csMsgSISpec;
	CString m_csMsgDelnr;

protected:
	CXTResizeGroupBox m_wndGrp1;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;

	CMyExtEdit m_wndEdit3;		//fastighets-/best�ndsnamn
	CMyExtEdit m_wndEdit1;		//block
	CMyExtEdit m_wndEdit2;		//enhet
	CMyExtEdit m_wndEdit4;		//areal
	CMyExtEdit m_wndEdit5;		//breddgrad
	CMyExtEdit m_wndEdit6;		//h�jd �ver hav
	CMyExtEdit m_wndEdit7;		//best�nds�lder
	COXMaskedEdit m_wndEdit8;	//H100
	CMyExtEdit m_wndEdit9;		//Sida 1 eller 2 vid breddning
	CMyExtEdit m_wndEdit10;		//Delnr
	
	CXTButton m_wndBtnOpenPropWnd;

	void setLanguage(void);
	BOOL m_bDataNotChanged;

public:
	CGeneralDataDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGeneralDataDlg();

// Dialog Data
	enum { IDD = IDD_DIALOGBAR };

	//prevent Enter and Cancel on dialog
	virtual void OnOK() {}
	virtual void OnCancel() {}

	void clearDlg(void)
	{
		m_wndEdit1.SetWindowText(_T(""));
		m_wndEdit2.SetWindowText(_T(""));
		m_wndEdit3.SetWindowText(_T(""));
		m_wndEdit4.SetWindowText(_T(""));
		m_wndEdit5.SetWindowText(_T(""));
		m_wndEdit6.SetWindowText(_T(""));
		m_wndEdit7.SetWindowText(_T(""));
		m_wndEdit8.SetWindowText(_T(""));
		m_wndEdit9.SetWindowText(_T(""));
		m_wndEdit10.SetWindowText(_T(""));
	}

	CString getNumber(void) {return m_wndEdit1.getText();}
	CString getSubNumber(void) {return m_wndEdit2.getText();}
	CString getName(void) {return m_wndEdit3.getText();}
	CString getArea(void) {return m_wndEdit4.getText();}
	CString getLatitude(void) {return m_wndEdit5.getText();}
	CString getHeightOverSea(void) {return m_wndEdit6.getText();}
	CString getAge(void) {return m_wndEdit7.getText();}
	CString getH100(void) {return m_wndEdit8.GetInputData();}
	CString getSide(void) {return m_wndEdit9.getText();}
	CString getDelnr(void) {return m_wndEdit10.getText();}

	void setProperty(LPCTSTR sName);
	void setNumber(LPCTSTR sNum);
	void setSubNumber(LPCTSTR sNum);
	void setName(LPCTSTR sName);
	void setArea(int nArea);
	void setLatitude(int nLatitude);
	void setHeightOverSea(int nHgtSea);
	void setAge(int nAge);
	void setH100(LPCTSTR sSi);
	void setSide(int nSide);
	void setDelnr(LPCTSTR sNum);
	void setUpdate(BOOL bUpd) {UpdateData(bUpd);}

	BOOL isOkToClose(void) { return m_bDataNotChanged;}	
	void setDataChanged(void) { m_bDataNotChanged = FALSE;}
	void resetDataChanged(void) {m_bDataNotChanged = TRUE;}

	// -------------------------------------
  // the following code declares the 
  // required elements for our AnchorMap
  // -------------------------------------
  DECLARE_ANCHOR_MAP();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnEnChangeEdit1();
	afx_msg void OnEnChangeEdit2();
	afx_msg void OnEnChangeEdit3();
	afx_msg void OnEnChangeEdit4();
	afx_msg void OnEnChangeEdit5();
	afx_msg void OnEnChangeEdit6();
	afx_msg void OnEnChangeEdit7();
	afx_msg void OnEnChangeEdit8();
	afx_msg void OnEnChangeEdit9();
	afx_msg void OnEnChangeEdit10();
	afx_msg void OnBnClickedBtnOpenprop();
	afx_msg void OnEnKillfocusEdit10();
};
