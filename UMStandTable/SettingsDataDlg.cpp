// SettingsDataDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SettingsDataDlg.h"
#include "ResLangFileReader.h"
#include "MDIStandTableFrame.h"
#include "StandTableView.h"


// CSettingsDataDlg dialog

IMPLEMENT_DYNAMIC(CSettingsDataDlg, CDialog)

CSettingsDataDlg::CSettingsDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSettingsDataDlg::IDD, pParent)
{

}

CSettingsDataDlg::~CSettingsDataDlg()
{
}

void CSettingsDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_GRP1, m_wndGrp1);

	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);

	DDX_Control(pDX, IDC_BUTTON1, m_wndBtn1);
	//DDX_Control(pDX, IDC_BUTTON2, m_wndBtn2);
}


BEGIN_MESSAGE_MAP(CSettingsDataDlg, CDialog)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	//ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	//ON_EN_CHANGE(IDC_EDIT1, &CSettingsDataDlg::OnEnChangeEdit1)
END_MESSAGE_MAP()

// ------------------------------------------------
// The following code declares our AnchorMap
// and defines how each control should be handled
// ------------------------------------------------
BEGIN_ANCHOR_MAP(CSettingsDataDlg)
	ANCHOR_MAP_ENTRY(IDC_LBL1, ANF_TOP|ANF_LEFT)
	ANCHOR_MAP_ENTRY(IDC_EDIT1, ANF_TOP|ANF_LEFT)
	//ANCHOR_MAP_ENTRY(IDC_BUTTON1, ANF_TOP|ANF_LEFT)
	//ANCHOR_MAP_ENTRY(IDC_BUTTON2, ANF_TOP|ANF_LEFT)
END_ANCHOR_MAP()

// CSettingsDataDlg message handlers

BOOL CSettingsDataDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sAbrevLangSet = getLangSet();
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	setLanguage();

	m_wndEdit1.SetEnabledColor(BLACK, WHITE);
	m_wndEdit1.SetDisabledColor(BLACK, INFOBK);
	
	m_wndEdit1.SetAsNumeric();
	m_wndEdit1.setFloat(/*2.0*/fDefDclsValue, 1);

	

	// --------------------------------------
	// At this point, we�ve set everything
	// up for our dialog except the 
	// anchoring/docking. We�ll do this now
	// with a call to InitAnchors()
	// --------------------------------------
	InitAnchors();


	return 0;
}

BOOL CSettingsDataDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CDialog::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSettingsDataDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect rcWnd;
	GetWindowRect(&rcWnd);
	HandleAnchors(&rcWnd);
	Invalidate(false);
}

BOOL CSettingsDataDlg::OnEraseBkgnd(CDC* pDC)
{
	// Here we call the EraseBackground-Handler from the
  // anchor-map which will reduce the flicker.  

	return(m_bpfxAnchorMap.EraseBackground(pDC->m_hDC));
}

void CSettingsDataDlg::setLanguage()
{
	if(fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if(xml->Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml->str(IDS_STRING111));

			m_wndBtn1.SetWindowText(xml->str(IDS_STRING112));
			//m_wndBtn2.SetWindowText(xml->str(IDS_STRING113));
		}
		delete xml;
	}
}

void CSettingsDataDlg::OnBnClickedButton1()
{
	//check that value inside limits
	if(m_wndEdit1.getFloat() > MAX_DCLS)
	{
		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING115));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
		return;
	}//if(m_wndEdit1.getInt() > MAX_DCLS)

	if(m_wndEdit1.getFloat() < MIN_DCLS)
	{
		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING116));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
		return;
	}//if(m_wndEdit1.getFloat() < MIN_DCLS)



	if(m_wndEdit1.getFloat() >= 0.1)
	{
		CFrameWnd *pFrameWnd = STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd());
		if(pFrameWnd)
		{
			CMDIFrameDoc *pDoc = (CMDIFrameDoc*)pFrameWnd->GetActiveFrame()->GetActiveDocument();
			if(pDoc)
			{
				POSITION pos = pDoc->GetFirstViewPosition();
				if(pos)
				{
					CStandTableView *pView = (CStandTableView*)pDoc->GetNextView(pos);
					if(pView)
					{
						pView->setupDclsTables(m_wndEdit1.getFloat());
					}//if(pView)
				}//if(pos)
			}//if(pDoc)
		}//if(pFrameWnd)
	}//if(m_wndEdit1.getInt() > 0)
	else
	{
		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader();
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING114));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
	}//else
}


void CSettingsDataDlg::OnEnChangeEdit1()
{
	// needs to do this check somewhere else
	if(m_wndEdit1.getFloat() > MAX_DCLS)
	{
		m_wndEdit1.setFloat(MAX_DCLS,1);

		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING115));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
	}//if(m_wndEdit1.getInt() > MAX_DCLS)

	if(m_wndEdit1.getFloat() < MIN_DCLS)
	{
		m_wndEdit1.setFloat(MIN_DCLS,1);

		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING116));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
	}//if(m_wndEdit1.getFloat() < MIN_DCLS)

}

double CSettingsDataDlg::getAndCheckDcls()
{
	if(m_wndEdit1.getFloat() > MAX_DCLS)
	{
		m_wndEdit1.setFloat(MAX_DCLS,1);

		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING115));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
		return -1.0;
	}//if(m_wndEdit1.getInt() > MAX_DCLS)

	if(m_wndEdit1.getFloat() < MIN_DCLS)
	{
		m_wndEdit1.setFloat(MIN_DCLS,1);

		if(fileExists(m_sLangFN))
		{
			RLFReader *xml = new RLFReader;
			if(xml->Load(m_sLangFN))
			{
				AfxMessageBox(xml->str(IDS_STRING116));
			}
			delete xml;
		}//if(fileExists(m_sLangFN))
		return -1.0;
	}//if(m_wndEdit1.getFloat() < MIN_DCLS)

	return m_wndEdit1.getFloat();
}